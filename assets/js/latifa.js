$(document).ready(function() {
	$('select[name="group"]').on('click', function() {
		var groupId = $(this).val();

		if (groupId) {
			$.ajax({
				url: '/latifa/ajax/subgroup',
				type: 'POST',
				dataType: 'json',
				data: {
					group_id: groupId
				},
				success: function(data) {
					$('select[name="subgroup"]').empty();
					$.each(data.items, function(key, value) {
						$('select[name="subgroup"]').append(
							'<option value="' + value.subgroup_id + '">' + value.subgroup_name + '</option>'
						);
					});
				}
			});
		} else {
			$('select[name="subgroup"]').empty();
		}
	});

	$('#btnBarangInput').on('click', function() {
		console.log('test');
		var groupId = $(this).val();

		if (groupId) {
			$.ajax({
				url: '/latifa/ajax/subgroup',
				type: 'POST',
				dataType: 'json',
				data: {
					group_id: groupId
				},
				success: function(data) {
					$('select[name="subgroup"]').empty();
					$.each(data.items, function(key, value) {
						$('select[name="subgroup"]').append(
							'<option value="' + value.subgroup_id + '">' + value.subgroup_name + '</option>'
						);
					});
				}
			});
		} else {
			$('select[name="subgroup"]').empty();
		}
	});

	$('#tableBarangPembelian').DataTable({
		createdRow: function(row, data, index) {
			$('td', row).css({
				cursor: 'pointer'
			});
		}
	});

	$('#tableBarangPembelian td').dblclick(function() {
		var row = $(this).closest('tr');
		var barang_id = row.find('td:nth-child(1)');
		$.each(barang_id, function() {
			var_barang_id = $(this).text();
		});

		$('#barangId').val(var_barang_id);
		$('#modalBarangPembelian').modal('hide');
	});

	$('#tableBarangPermintaan').DataTable({
		createdRow: function(row, data, index) {
			$('td', row).css({
				cursor: 'pointer'
			});
		}
	});

	$('#tableBarangPermintaan td').dblclick(function() {
		var row = $(this).closest('tr');
		var barang_id = row.find('td:nth-child(1)');
		$.each(barang_id, function() {
			var_barang_id = $(this).text();
		});

		$('#barangIdPermintaan').val(var_barang_id);
		$('#modalBarangPermintaan').modal('hide');
	});

	$('#tableBarangCard td').dblclick(function() {
		var row = $(this).closest('tr');
		var barang_id = row.find('td:nth-child(1)');
		$.each(barang_id, function() {
			var_barang_id = $(this).text();
		});

		$('#barangIdCard').val(var_barang_id);
		$('#modalCard').modal('hide');
	});

	// $('#generateSubmit').on('click', function(e) {
	// 	// e.preventDefault();
	// 	$.ajax({
	// 		beforeSend: function(xhr, opts){
	// 			$( "#loading" ).show();
	// 		},
	// 		success: function(){

	// 		},
	// 		complete : function() {
	// 			$( "#loading" ).hide();
	// 		}
	// 	});
	// });
	$('.table-data').DataTable();
});

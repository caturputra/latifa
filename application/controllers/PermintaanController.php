<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk menangani permintaan
 * barang dari farmasi ke gudang
 */
class PermintaanController extends MY_Contoller 
{
    private $_view = 'permintaan/';
    protected $scope = 'admin, operator';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('permintaan');
        $this->load->model('dana');
        $this->load->model('group');
        $this->load->model('barang');
        $this->load->model('condition');
        $this->load->helper('autonum');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->permintaan;

        $data = [
            'title' => 'permintaan',
            'models' => $model->findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->permintaan;
        $user_id = $this->session->userdata['user_id'];
        $unit_id = $this->session->userdata['unit_id'];
        $hostname = gethostname();
        $now = strtotime('now');

        if ($this->input->post()) {

            $this->db->trans_begin();

            $tahun_anggaran = $this->input->post('tahun_anggaran', true);
            $sumber_dana = $this->input->post('dana', true);
            $subgroup = $this->input->post('subgroup', true);
            $supplier = null;
            $now = strtotime('now');
            $tgl_dokumen = date('Y-m-d', $now);

            $no_dokumen = generateAutonum('trx_persediaan', 'psd_nodokumen', [
                'key' => 'psd_nodokumen',
                'val' => date('Ymd') . '.PEN.',
                'place' => 'after'
            ], 13, 5);

            $permintaan_temp = $this->db->select('pt.barang_id, pt.tanggal, pt.psd_kredit, pt.user_id,  pt.session_id, pt.hostname')
                            ->from('permintaan_temp pt')
                            ->where('user_id', $user_id)
                            ->where('session_id', $this->session->session_id)
                            ->where('hostname', $hostname)
                            ->get();

            foreach($permintaan_temp->result_array() as $key => $row) {
                //mengambil stok total yang dimiliki
                $old_stock = $this->db->select('barang_id, barang_stock')
                ->from('mst_barang')
                ->where('barang_id', $row['barang_id'])
                ->get()
                ->result_array();

                //jika jumlah beli kurang dari total stok dimiliki
                if ($row['psd_kredit'] <= $old_stock[0]['barang_stock']) {
                    $temp_stock = $this->db->select('count(barang_id) as jumlah, barang_tahunanggaran, unit_id, subgroup_id, barang_id, barang_expdate, barang_hargasatuan, barang_hargajual, dana_id, condition_id, supplier_id')
                    ->from('trx_barang')
                    ->where('barang_id', $old_stock[0]['barang_id'])
                    ->where('barang_status', '1')
                    ->where('condition_id', '1')
                    ->group_by(['barang_hargasatuan', 'barang_tahunanggaran', 'dana_id', 'barang_tanggal'])
                    ->order_by('barang_tahunanggaran ASC, barang_tanggal ASC, barang_expdate ASC')
                    ->get();

                    $temp_jumlah_beli = intval($row['psd_kredit']);

                    foreach ($temp_stock->result_array() as $key => $temp_stock_2) {
                        $temp_stock_sisa = intval($temp_stock_2['jumlah']);

                        if ($temp_stock_sisa > 0) {
                            
                            if ($temp_jumlah_beli <= $temp_stock_sisa) {
                                $jumlah = $temp_jumlah_beli;
                            } else {
                                $jumlah = $temp_stock_sisa;
                            }

                            for($i = 1; $i <= $jumlah; $i++) {
                                $query_insert_trxbarang = " INSERT INTO trx_barang(barang_tahunanggaran, unit_id, subgroup_id, barang_id, barang_expdate, barang_hargasatuan, barang_hargajual, dana_id, created_at, barang_tanggal, barang_status, barang_nodokumen, condition_id, supplier_id) VALUES(
                                    '{$temp_stock_2['barang_tahunanggaran']}',
                                    'F001',
                                    '{$temp_stock_2['subgroup_id']}',
                                    '{$temp_stock_2['barang_id']}',
                                    '{$temp_stock_2['barang_expdate']}',
                                    '{$temp_stock_2['barang_hargasatuan']}',
                                    '{$temp_stock_2['barang_hargajual']}',
                                    '{$temp_stock_2['dana_id']}',
                                    '{$now}',
                                    '{$tgl_dokumen}',
                                    '2',
                                    '{$no_dokumen}',
                                    '{$temp_stock_2['condition_id']}',
                                    '{$temp_stock_2['supplier_id']}'
                                    )
                                ";

                                $this->db->query($query_insert_trxbarang);
                                $temp_stock_sisa--;
                                $temp_jumlah_beli--; 
                            }
                            
                            //insert trxpersediaan
                            $persediaan = " INSERT INTO trx_persediaan(trx_id, barang_id, dana_id, psd_tahunperolehan, psd_lokasi, psd_jumlah, psd_debet, psd_kredit, psd_harga, psd_hargajual, psd_d_k, psd_nodokumen, psd_tgldokumen, psd_keterangan, supplier_id, user_id, created_at) 
                            VALUES (
                                '201',
                                '{$row['barang_id']}',
                                '{$sumber_dana}',
                                '{$tahun_anggaran}',
                                '{$unit_id}',
                                '{$row['psd_kredit']}',
                                '0',
                                '{$row['psd_kredit']}',
                                '0',
                                '0',
                                'K',
                                '{$no_dokumen}',
                                '{$tgl_dokumen}',
                                'permintaan dari " . $this->session->userdata['user_unit'] . "',
                                '{$temp_stock_2['supplier_id']}',
                                '{$user_id}',
                                '{$now}'
                                )
                            ";

                            $sql_delete_temp = " DELETE FROM permintaan_temp WHERE barang_id = '{$row['barang_id']}' AND user_id = '{$user_id}' AND session_id = '{$row['session_id']}' AND hostname = '{$row['hostname']}'
                            ";

                            $this->db->query($sql_delete_temp);
                            $this->db->query($persediaan);
                        } else {
                            $this->db->trans_rollback();
                        }
                    }
                } else {
                    $this->db->trans_rollback();
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }
        }

        $permintaan_temp = $this->db->select('b.barang_name, pt.barang_id, pt.tanggal, pt.psd_kredit, pt.user_id, pt.session_id, pt.hostname')
                            ->from('permintaan_temp pt')
                            ->join('mst_barang b', 'b.barang_id = pt.barang_id')
                            ->where('pt.user_id', $user_id)
                            ->where('pt.session_id', $this->session->session_id)
                            ->where('pt.hostname', $hostname)
                            ->get()
                            ->result_array();

        $data = [
            'permintaans' => $model->findAll(),
            'danas' => Dana::findAll(),
            'groups' => Group::findAll(),
            'conditions' => Condition::findAll(),
            'shows_barang' => Barang::showBarangList(),
            'barangs' => isset($permintaan_temp) ? $permintaan_temp : [],
            'title' => 'Tambah permintaan',
        ];

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->permintaan;

        $data = [
            'permintaans' => $model->findAll(),
            'permintaan' => $model->findOne($id),
            'title' => 'Edit jenis permintaan',
        ];

        if ($this->input->post()) {
            if ($model->update()) {
                $this->flash->setFlash('Data berhasil disimpan.');
                redirect('permintaan/index');
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->permintaan;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('permintaan/index');
        }
    }

    public function setBarang()
    {
        $date = strtotime('now');
        $barang_id = $this->input->post('barang_id', true);
        $jumlah = $this->input->post('jumlah', true);
        $user_id = $this->session->userdata['user_id'];
        $session_id = $this->session->session_id;
        $hostname = gethostname();

        $insert_temp = "
            insert into permintaan_temp(barang_id, psd_kredit, tanggal, user_id, session_id, hostname)
            values(
                '{$barang_id}',
                '{$jumlah}',
                '{$date}',
                '{$user_id}',
                '{$session_id}',
                '{$hostname}'
            )
        ";

        $this->db->query($insert_temp);

        redirect('permintaan/create');
    }
}
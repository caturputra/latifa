<?php defined('BASEPATH') or exit('No direct script access allowed');

class AuthController extends CI_Controller
{
    private $_view = 'auth/';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
    }

    public function login() {
        
        return $this->load->view($this->_view.'login');
    }
}
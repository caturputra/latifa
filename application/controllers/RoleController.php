<?php defined('BASEPATH') or exit('No direct script access allowed');

class RoleController extends MY_Contoller
{
    private $_view = 'role/';
    protected $scope = 'admin';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('role');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->role;

        $data = [
            'title' => 'Role',
            'models' => Role::findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->role;

        $data = [
            'roles' => $model->findAll(),
            'title' => 'Tambah jenis role',
        ];
        
        if ($this->input->post()) {
            if ($model->create()) {
                $this->flash->setFlash('Data berhasil disimpan.');
                redirect('role/index');
            }
        }

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->role;

        $data = [
            'roles' => $model->findAll(),
            'role' => $model->findOne($id),
            'title' => 'Edit jenis role',
        ];

        if ($this->input->post()) {
            if ($model->update()) {
                $this->flash->setFlash('Data berhasil disimpan.');
                redirect('role/index');
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->role;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('role/index');
        }
    }
}
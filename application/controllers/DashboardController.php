<?php defined('BASEPATH') or exit('No direct script access allowed');

class DashboardController extends MY_Contoller 
{
    private $_view = 'dashboard/';
    protected $scope = 'admin, operator, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $data = [
            'title' => 'Dashboard',
        ];

        $this->view->load($this->_view.'index', $data);
    }

}
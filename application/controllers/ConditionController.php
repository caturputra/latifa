<?php defined('BASEPATH') or exit('No direct script access allowed');

class ConditionController extends MY_Contoller 
{
    private $_view = 'condition/';
    protected $scope = 'admin, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('condition');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->condition;

        $data = [
            'title' => 'Condition',
            'models' => $model->findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->condition;

        $data = [
            'conditions' => $model->findAll(),
            'title' => 'Tambah jenis condition',
        ];
        
        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->create()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('condition/index');
                }
            }
        }

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->condition;

        $data = [
            'conditions' => $model->findAll(),
            'condition' => $model->findOne($id),
            'title' => 'Edit jenis condition',
        ];

        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->update()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('condition/index');
                }
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->condition;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('condition/index');
        }
    }
}
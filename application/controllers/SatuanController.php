<?php defined('BASEPATH') or exit('No direct script access allowed');

class SatuanController extends MY_Contoller 
{
    private $_view = 'satuan/';
    protected $scope = 'admin, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('satuan');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->satuan;

        $data = [
            'title' => 'satuan',
            'models' => $model->findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->satuan;

        $data = [
            'satuans' => $model->findAll(),
            'title' => 'Tambah jenis satuan',
        ];
        
        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->create()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('satuan/index');
                }
            }
        }

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->satuan;

        $data = [
            'satuans' => $model->findAll(),
            'satuan' => $model->findOne($id),
            'title' => 'Edit jenis satuan',
        ];

        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->update()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('satuan/index');
                }
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->satuan;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('satuan/index');
        }
    }
}
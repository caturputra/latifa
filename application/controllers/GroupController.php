<?php defined('BASEPATH') or exit('No direct script access allowed');

class groupController extends MY_Contoller
{
    private $_view = 'group/';
    protected $scope = 'admin, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('group');
        $this->load->model('subgroup');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->group;

        $data = [
            'title' => 'group',
            'models' => $model->findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->group;

        $data = [
            'groups' => $model->findAll(),
            'title' => 'Tambah jenis group',
        ];
        
        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->create()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('group/index');
                }
            }
        }

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->group;

        $data = [
            'groups' => $model->findAll(),
            'group' => $model->findOne($id),
            'title' => 'Edit jenis group',
        ];

        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->update()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('group/index');
                }
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->group;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('group/index');
        }
    }

    public function subgroup($id)
    {
        $model = $this->subgroup;
        $data = [
            'title' => 'Subgroup',
            'group_id' => $id,
            'subgroups' => Subgroup::findByGroup($id),
        ];
        $this->view->load('subgroup/index' ,  $data);
    }

    public function createSubgroup($id)
    {
        $model = $this->subgroup;

        $data = [
            'group_id' => $id,
            'title' => 'Tambah jenis subgroup',
        ];
        
        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load('subgroup/create', $data);
            } else {
                if ($model->create()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('group/'. $id .'/subgroup/index');
                }
            }
        }

        $this->view->load('subgroup/create', $data);
    }
}
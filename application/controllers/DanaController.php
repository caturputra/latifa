<?php defined('BASEPATH') or exit('No direct script access allowed');

class DanaController extends MY_Contoller 
{
    private $_view = 'dana/';
    protected $scope = 'admin, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dana');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->dana;

        $data = [
            'title' => 'Dana',
            'models' => $model->findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->dana;

        $data = [
            'danas' => $model->findAll(),
            'title' => 'Tambah jenis dana',
        ];
        
        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->create()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('dana/index');
                }
            }
        }

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->dana;

        $data = [
            'danas' => $model->findAll(),
            'dana' => $model->findOne($id),
            'title' => 'Edit jenis dana',
        ];

        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->update()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('dana/index');
                }
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->dana;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('dana/index');
        }
    }
}
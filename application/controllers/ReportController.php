<?php defined('BASEPATH') or exit('No direct script access allowed');

class ReportController extends MY_Contoller 
{
    private $_view = 'report/';
    protected $scope = 'admin, operator, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('barang');
        $this->load->model('group');
        $this->load->model('satuan');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->barang;

        $data = [
            'title' => 'Laporan',
            'models' => $model->findAll(),
            'shows_barang' => Barang::showBarangList(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    
}
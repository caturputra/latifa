<?php defined('BASEPATH') or exit('No direct script access allowed');

class PembelianController extends MY_Contoller 
{
    private $_view = 'pembelian/';
    protected $scope = 'admin, operator, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian');
        $this->load->model('dana');
        $this->load->model('group');
        $this->load->model('barang');
        $this->load->model('condition');
        $this->load->model('supplier');
        $this->load->helper('autonum');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->pembelian;

        $data = [
            'title' => 'pembelian',
            'models' => $model->findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->pembelian;
        $user_id = $this->session->userdata['user_id'];
        $unit_id = $this->session->userdata['unit_id'];
        $hostname = gethostname();
        $now = strtotime('now');

        if ($this->input->post()) {

            $this->db->trans_begin();

            $tahun_anggaran = $this->input->post('tahun_anggaran', true);
            $sumber_dana = $this->input->post('dana', true);
            $group = $this->input->post('group', true);
            $subgroup = $this->input->post('subgroup', true);
            $tgl_dokumen = date('Y-m-d', $now);

            $no_dokumen = generateAutonum('trx_persediaan', 'psd_nodokumen', [
                'key' => 'psd_nodokumen',
                'val' => date('Ymd') . '.PEM.',
                'place' => 'after'
            ], 13, 5);

            $pembelian_temp = $this->db->select('*')
                            ->from('pembelian_temp')
                            ->where('user_id', $user_id)
                            ->where('session_id', $this->session->session_id)
                            ->where('hostname', $hostname)
                            ->get();
            
            foreach($pembelian_temp->result_array() as $key => $row) {
                for ($i = 1; $i <= $row['psd_debet']; $i++) {
                    $trx_barang = " INSERT INTO trx_barang(barang_tahunanggaran, unit_id, subgroup_id, barang_id, barang_expdate, barang_hargasatuan, barang_hargajual, dana_id, created_at, barang_tanggal, barang_nodokumen, condition_id, supplier_id) VALUES(
                            '{$tahun_anggaran}',
                            '{$unit_id}',
                            '{$subgroup}',
                            '{$row['barang_id']}',
                            '{$row['barang_expdate']}',
                            '{$row['barang_hargasatuan']}',
                            '{$row['barang_hargajual']}',
                            '{$sumber_dana}',
                            '{$now}',
                            '{$tgl_dokumen}',
                            '{$no_dokumen}',
                            '{$row['condition_id']}',
                            '{$row['supplier_id']}'
                        )
                    ";
                    $this->db->query($trx_barang);
                }

                $persediaan = " INSERT INTO trx_persediaan(trx_id, barang_id, dana_id, psd_tahunperolehan, psd_lokasi, psd_jumlah, psd_debet, psd_kredit, psd_harga, psd_hargajual, psd_d_k, psd_nodokumen, psd_tgldokumen, psd_keterangan, supplier_id, user_id, created_at) 
                VALUES (
                    '101',
                    '{$row['barang_id']}',
                    '{$sumber_dana}',
                    '{$tahun_anggaran}',
                    '{$unit_id}',
                    '{$row['psd_debet']}',
                    '{$row['psd_debet']}',
                    '0',
                    '{$row['barang_hargasatuan']}',
                    '{$row['barang_hargajual']}',
                    'D',
                    '{$no_dokumen}',
                    '{$tgl_dokumen}',
                    'Pembelian dari supplier',
                    '{$row['supplier_id']}',
                    '{$user_id}',
                    '{$now}'
                    )
                ";

                $sql_delete_temp = " DELETE FROM pembelian_temp WHERE barang_id = '{$row['barang_id']}' AND user_id = '{$user_id}' AND session_id = '{$row['session_id']}' AND hostname = '{$row['hostname']}'
                ";

                $this->db->query($sql_delete_temp);
                $this->db->query($persediaan);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }
        }

        $pembelian_temp = $this->db->select('*')
                            ->from('pembelian_temp')
                            ->where('user_id', $user_id)
                            ->where('session_id', $this->session->session_id)
                            ->where('hostname', $hostname)
                            ->get()
                            ->result_array();

        $data = [
            'pembelians' => $model->findAll(),
            'danas' => Dana::findAll(),
            'groups' => Group::findAll(),
            'conditions' => Condition::findAll(),
            'shows_barang' => Barang::showBarangList(),
            'suppliers' => Supplier::findAll(),
            'barangs' => isset($pembelian_temp) ? $pembelian_temp : [],
            'title' => 'Tambah pembelian',
        ];

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->pembelian;

        $data = [
            'pembelians' => $model->findAll(),
            'pembelian' => $model->findOne($id),
            'title' => 'Edit jenis pembelian',
        ];

        if ($this->input->post()) {
            if ($model->update()) {
                $this->flash->setFlash('Data berhasil disimpan.');
                redirect('pembelian/index');
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->pembelian;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('pembelian/index');
        }
    }

    public function setBarang()
    {
        $date = strtotime($this->input->post('date', true));
        $barang_id = $this->input->post('barang_id', true);
        $condition = $this->input->post('condition', true);
        $exp_date = strtotime($this->input->post('exp_date', true));
        $harga_satuan = $this->input->post('harga_satuan', true);
        $harga_jual = $this->input->post('harga_jual', true);
        $jumlah = $this->input->post('jumlah', true);
        $supplier = $this->input->post('supplier', true);
        $user_id = $this->session->userdata['user_id'];
        $session_id = $this->session->session_id;
        $hostname = gethostname();

        $insert_temp = "
            insert into pembelian_temp(barang_id, condition_id, barang_expdate, barang_hargasatuan, barang_hargajual, psd_debet, tanggal, user_id, session_id, hostname, supplier_id)
            values(
                '{$barang_id}',
                '{$condition}',
                '{$exp_date}',
                '{$harga_satuan}',
                '{$harga_jual}',
                '{$jumlah}',
                '{$date}',
                '{$user_id}',
                '{$session_id}',
                '{$hostname}',
                '{$supplier}'
            )
        ";

        $this->db->query($insert_temp);

        redirect('pembelian/create');
    }
}
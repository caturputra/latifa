<?php defined('BASEPATH') or exit('No direct script access allowed');

class BarangController extends MY_Contoller 
{
    private $_view = 'barang/';
    protected $scope = 'admin, operator, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('barang');
        $this->load->model('group');
        $this->load->model('satuan');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->barang;

        $data = [
            'title' => 'Barang',
            'models' => $model->findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->barang;

        $data = [
            'barangs' => $model->findAll(),
            'groups' => Group::findAll(),
            'satuans' => Satuan::findAll(),
            'title' => 'Tambah barang',
        ];
        
        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->create()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('barang/index');
                }
            }
        }

        return $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->barang;

        $data = [
            'barangs' => $model->findAll(),
            'barang' => $model->findOne($id),
            'groups' => Group::findAll(),
            'satuans' => Satuan::findAll(),
            'title' => 'Edit jenis barang',
        ];

        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->update()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('barang/index');
                }
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->barang;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('barang/index');
        }
    }
}
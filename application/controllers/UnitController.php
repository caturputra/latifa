<?php defined('BASEPATH') or exit('No direct script access allowed');

class UnitController extends MY_Contoller 
{
    private $_view = 'unit/';
    protected $scope = 'admin, kagudang';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('unit');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->unit;

        $data = [
            'title' => 'Unit',
            'models' => $model->findAll(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->unit;

        $data = [
            'units' => $model->findAll(),
            'title' => 'Tambah jenis unit',
        ];
        
        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->create()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('unit/index');
                }
            }
        }

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->unit;

        $data = [
            'units' => $model->findAll(),
            'unit' => $model->findOne($id),
            'title' => 'Edit jenis unit',
        ];

        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->update()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('unit/index');
                }
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->unit;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('unit/index');
        }
    }
}
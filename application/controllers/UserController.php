<?php defined('BASEPATH') or exit('No direct script access allowed');

class UserController extends MY_Contoller 
{
    private $_view = 'user/';
    protected $scope = 'admin';

    /**
     * init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('unit');
        $this->load->model('role');
    }

    /**
     * Show all data
     * 
     * @return array
     */
    public function index()
    {
        $model = $this->user;

        $data = [
            'title' => 'User',
            'models' => User::profile(),
        ];

        $this->view->load($this->_view.'index', $data);
    }

    /**
     * Create a model
     */
    public function create()
    {
        $model = $this->user;

        $data = [
            'users' => $model->findAll(),
            'title' => 'Tambah jenis user',
            'units' => Unit::findAll(),
            'roles' => Role::findAll(),
        ];
        
        if ($this->input->post()) {
            if (!$model->validate()) {
                return $this->view->load($this->_view.'create', $data);
            } else {
                if ($model->create()) {
                    $this->flash->setFlash('Data berhasil disimpan.');
                    redirect('user/index');
                }
            }
        }

        $this->view->load($this->_view.'create', $data);
    }

    /**
     * Update a model
     */
    public function update($id)
    {
        $model = $this->user;

        $data = [
            'users' => $model->findAll(),
            'user' => $model->findOne($id),
            'title' => 'Edit jenis user',
        ];

        if ($this->input->post()) {
            if ($model->update()) {
                $this->flash->setFlash('Data berhasil disimpan.');
                redirect('user/index');
            }
        }

        $this->view->load($this->_view.'update' ,  $data);
    }

    /**
     * Delete a model
     */
    public function delete($id)
    {
        if (!isset($id)) {
            show_404();
        }

        $model = $this->user;
        if ($model->delete($id)) {
            $this->flash->setFlash('Data berhasil dihapus.');
            redirect('user/index');
        }
    }
}
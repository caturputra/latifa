<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(['form_validation']);
	}

	/**
	 * Log the user in
	 */
	public function login()
	{
		if ($this->auth->is_loggedin()) {
			return redirect('dashboard', 'refresh');
		}

		$this->auth->login();

		$this->load->view('auth/login');
	}

	/**
	 * Log the user out
	 */
	public function logout()
	{
		if($this->auth->logout()) {
			return redirect('auth/signin', 'refresh');
		}

        return false;
	}

}

<?php defined('BASEPATH') or exit('No direct script access allowed');

class AjaxController extends CI_Controller
{
    public function dropdownSubgroup() {
        $group_id = $this->input->post('group_id', true);

        $query = $this->db->select('subgroup_id, UPPER(subgroup_name) as subgroup_name')
                        ->from('mst_subgroup')
                        ->where('group_id', $group_id)
                        ->get()
                        ->result_array();
        
        echo json_encode(['items' => $query]);
    }
}
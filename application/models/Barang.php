<?php defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends CI_Model
{
    //set table name
    private static $_table = 'mst_barang';
    private static $db;

    //set names of field
    public $barang_id;
    public $barang_name;
    public $barang_satuan;
    public $user_id;
    public $subgroup_id;
    public $created_at;

    /**
     * Init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('autonum');
        $this->load->library('form_validation');
        self::$db = &get_instance()->db;
    }

    public function rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'Nama barang',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'satuan',
                'label' => 'satuan barang',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'subgroup',
                'label' => 'sub kelompok barang',
                'rules' => 'trim|required',
            ],
        ];
    }

    public function validate()
    {
        $validate = $this->form_validation;
        $validate->set_rules($this->rules());

        if ($validate->run()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Make a new model
     * 
     * @return mixed
     */
    public function create()
    {
        $name = $this->input->post('name', true);
        $satuan = $this->input->post('satuan', true);
        $user = $this->session->userdata['user_id'];
        $subgroup = $this->input->post('subgroup', true);
        $created_at = strtotime('now');

        $id = generateAutonum('mst_barang', 'barang_id', [
			'key' => 'barang_id',
			'val' => substr($name, 0, 3),
			'place' => 'after'
        ], 2, 2);
        
        $this->barang_id = $id;
        $this->barang_name = $name;        
        $this->barang_satuan = $satuan;
        $this->user_id = $user;
        $this->subgroup_id = $subgroup;
        $this->created_at = $created_at;

        $create = $this->db->insert(self::$_table, $this);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a category model
     * @param int id get from input hidden id
     * @return mixed
     */
    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $satuan = $this->input->post('satuan', true);
        $user = 1;
        $subgroup = 1;
        
        $this->barang_name = $name;        
        $this->barang_satuan = $satuan;
        $this->user_id = $user;
        $this->subgroup_id = $subgroup;

        $update = $this->db->update(self::$_table, [
            'barang_name' => $this->barang_name,
            'barang_satuan' => $this->barang_satuan,
            'user_id' => $this->user_id,
            'subgroup_id' => $this->subgroup_id
        ], [
            'barang_id' => $id
        ]);

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a category model
     * @param int id
     * @return mixed
     */
    public function delete($id)
    {
        $delete = $this->db->delete(self::$_table, ['barang_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Show all model
     */
    public static function findAll()
    {
        return self::$db->get(self::$_table)->result_array();
    }

    /**
     * Show a model by id
     * @param id
     * @return array
     */
    public static function findOne($id)
    {
        return self::$db->get_where(self::$_table, ['barang_id' => $id])->row_array();
    }

    public static function showBarangList()
    {
        $query = self::$db->select('s.satuan_name, g.group_name, sg.subgroup_name, b.barang_id, b.barang_name')
                ->from('mst_barang b')
                ->join('mst_satuan s', 's.satuan_id = b.barang_satuan')
                ->join('mst_subgroup sg', 'sg.subgroup_id = b.subgroup_id')
                ->join('mst_group g', 'g.group_id = sg.group_id')
                ->get()
                ->result_array();

        return $query;
    }
}
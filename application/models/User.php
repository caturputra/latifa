<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model
{
    private static $_table = 'mst_users';
    private static $db;

    public $user_id;
    public $user_name;
    public $user_fullname;
    public $user_email;
    public $user_address;
    public $user_contact;
    public $user_gender;
    public $user_unit;
    public $user_role;
    public $user_status;
    public $user_password;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        self::$db = &get_instance()->db;
        $this->load->library('form_validation');
    }

    public function rules()
    {
        return [
            [
                'field' => 'id',
                'label' => 'ID',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'name',
                'label' => 'Nama',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'address',
                'label' => 'Alamat',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'contact',
                'label' => 'Kontak',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'gender',
                'label' => 'Jenis Kelamin',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'unit',
                'label' => 'Unit',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'role',
                'label' => 'Role',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required',
            ],
        ];
    }

    public function validate()
    {
        $validate = $this->form_validation;
        $validate->set_rules($this->rules());

        if ($validate->run()) {
            return true;
        } else {
            return false;
        }
    }

    public function activate()
    {

    }

    public function create()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $email = $this->input->post('email', true);
        $address = $this->input->post('address', true);
        $contact = $this->input->post('contact', true);
        $gender = $this->input->post('gender', true);
        $unit = $this->input->post('unit', true);
        $role = $this->input->post('role', true);
        $status = $this->input->post('status', true);
        $password = $this->input->post('password', true);

        $this->user_id = $id;
        $this->user_name = $name;
        $this->user_fullname = $name;
        $this->user_email = $email;
        $this->user_address = $address;
        $this->user_contact = $contact;
        $this->user_gender = $gender;
        $this->user_unit = $unit;
        $this->user_role = $role;
        $this->user_status = $status;
        $this->user_password = $password;

        $create = $this->db->insert(self::$_table, $this);
        $this->db->insert('role_users', [
            'role_id' => $this->user_role,
            'user_id' => $this->user_id
        ]);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $email = $this->input->post('email', true);
        $address = $this->input->post('address', true);
        $contact = $this->input->post('contact', true);
        $gender = $this->input->post('gender', true);
        $unit = $this->input->post('unit', true);
        $role = $this->input->post('role', true);
        $status = $this->input->post('status', true);
        $password = $this->input->post('password', true);
        $repassword = $this->input->post('repassword', true);

        $this->user_id = $id;
        $this->user_name = $name;
        $this->user_fullname = $name;
        $this->user_email = $email;
        $this->user_address = $address;
        $this->user_contact = $contact;
        $this->user_gender = $gender;
        $this->user_unit = $unit;
        $this->user_role = $role;
        $this->user_status = $status;
        $this->user_password = $password;

        $update = $this->db->update(self::$_table, [
            'unit_name' => $this->unit_name,
            'unit_status' => $this->unit_status,
        ], [
            'unit_id' => $user_id
        ]);

        if ($update) {
            return true;
        } else {
            return false; 
        }
    }

    public function delete($id) {
        $delete = $this->db->delete(self::$_table, ['user_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    public static function findAll()
    {
        return self::$db->get(self::$_table)->result_array();
    }

    public static function findOne($user_id)
    {
        return self::$db->get_where(self::$_table, ['unit_id' => $user_id])->row_array();
    }

    public static function profile()
    {
        $user = self::$db->select('u.user_id, u.user_fullname, r.role_name, u.user_email, un.unit_name, u.user_gender, u.user_contact, u.user_status')
        ->from(self::$_table . ' as u')
        ->join('mst_role r', 'r.role_id = u.user_role', 'inner')
        ->join('mst_unit un', 'un.unit_id = u.user_unit', 'inner')
        ->get()
        ->result_array();

        return $user;
    }
}
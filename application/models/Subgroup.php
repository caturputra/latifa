<?php defined('BASEPATH') or exit('No direct script access allowed');

class Subgroup extends CI_Model
{
    //set table name
    private static $_table = 'mst_subgroup';
    private static $db;

    //set names of field
    public $subgroup_id;
    public $subgroup_name;
    public $group_id;

    /**
     * Init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('autonum');
        $this->load->library('form_validation');
        self::$db = &get_instance()->db;
    }

    public function rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'Nama',
                'rules' => 'trim|required',
            ],
        ];
    }

    public function validate()
    {
        $validate = $this->form_validation;
        $validate->set_rules($this->rules());

        if ($validate->run()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Make a new model
     * 
     * @return mixed
     */
    public function create()
    {
        $name = $this->input->post('name', true);
        $group = $this->input->post('group', true);

        $id = generateAutonum('mst_subgroup', 'subgroup_id', [
			'key' => 'subgroup_id',
			'val' => '',
			'place' => 'both'
        ], 0, 2);

        $this->subgroup_id = $id;
        $this->subgroup_name = $name;        
        $this->group_id = $group;

        $create = self::$db->insert(self::$_table, $this);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a category model
     * @param int id get from input hidden id
     * @return mixed
     */
    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);

        $this->subgroup_name = $name;

        $update = self::$db->update(self::$_table, [
            'subgroup_name' => $this->subgroup_name,
        ], [
            'subgroup_id' => $id
        ]);

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a category model
     * @param int id
     * @return mixed
     */
    public function delete($id)
    {
        $delete = self::$db->delete(self::$_table, ['subgroup_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Show all model
     */
    public static function findAll()
    {
        return self::$db->get(self::$_table)->result_array();
    }

    /**
     * Show a model by id
     * @param id
     * @return array
     */
    public static function findOne($id)
    {
        return self::$db->get_where(self::$_table, ['subgroup_id' => $id])->row_array();
    }

    public static function findByGroup($group)
    {
        return self::$db->get_where(self::$_table, ['group_id' => $group])->result_array();
    }
}
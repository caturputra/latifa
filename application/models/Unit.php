<?php defined('BASEPATH') or exit('No direct script access allowed');

class Unit extends CI_Model
{
    //set table name
    private static $_table = 'mst_unit';
    private static $db;

    //set names of field
    public $unit_id;
    public $unit_name;
    public $unit_status;

    /**
     * Init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('autonum');
        $this->load->library('form_validation');
        self::$db = &get_instance()->db;
    }


    public function rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'Nama',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required',
            ],
        ];
    }

    public function validate()
    {
        $validate = $this->form_validation;
        $validate->set_rules($this->rules());

        if ($validate->run()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Make a new model
     * 
     * @return mixed
     */
    public function create()
    {
        $name = $this->input->post('name', true);
        $status = $this->input->post('status', true);

        $id = generateAutonum('mst_unit', 'unit_id', [
			'key' => 'unit_id',
			'val' => substr($name, 0, 1),
			'place' => 'after'
        ], 1, 3);

        $this->unit_id = $id;
        $this->unit_status = $status;
        $this->unit_name = $name;   

        $create = $this->db->insert(self::$_table, $this);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a category model
     * @param int id get from input hidden id
     * @return mixed
     */
    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $status = $this->input->post('status', true);

        $this->unit_name = $name;
        $this->unit_status = $status;

        $update = $this->db->update(self::$_table, [
            'unit_name' => $this->unit_name,
            'unit_status' => $this->unit_status,
        ], [
            'unit_id' => $id
        ]);

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a category model
     * @param int id
     * @return mixed
     */
    public function delete($id)
    {
        $delete = $this->db->delete(self::$_table, ['unit_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Show all model
     */
    public static function findAll()
    {
        return self::$db->get(self::$_table)->result_array();
    }

    /**
     * Show a model by id
     * @param id
     * @return array
     */
    public static function findOne($id)
    {
        return self::$db->get_where(self::$_table, ['unit_id' => $id])->row_array();
    }
}
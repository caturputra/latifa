<?php defined('BASEPATH') or exit('No direct script access allowed');

class Group extends CI_Model
{
    //set table name
    private static $_table = 'mst_group';
    private static $db;

    //set names of field
    public $group_id;
    public $group_name;

    /**
     * Init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        self::$db = &get_instance()->db;
    }

    /**
     * Make a new model
     * 
     * @return mixed
     */
    public function create()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);

        $this->group_id = $id;
        $this->group_name = $name;        

        $create = $this->db->insert($this->_table, $this);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a category model
     * @param int id get from input hidden id
     * @return mixed
     */
    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);

        $this->group_name = $name;

        $update = $this->db->update($this->_table, [
            'group_name' => $this->group_name,
        ], [
            'group_id' => $id
        ]);

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a category model
     * @param int id
     * @return mixed
     */
    public function delete($id)
    {
        $delete = $this->db->delete($this->_table, ['group_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Show all model
     */
    public static function findAll()
    {
        return self::$db->get(self::$_table)->result_array();
    }

    /**
     * Show a model by id
     * @param id
     * @return array
     */
    public function findOne($id)
    {
        return $this->db->get_where($this->_table, ['group_id' => $id])->row_array();
    }
}
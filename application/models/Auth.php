<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        self::$db = &get_instance()->db;
    }

    public function hasRole()
    {
        $role = self::$db->select('role_name')
        ->from('mst_role')
        ->get()
        ->result_array();

        return $role;
    }
    
}
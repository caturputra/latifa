<?php defined('BASEPATH') or exit('No direct script access allowed');

class Permintaan extends CI_Model
{
    //set table name
    private $_table = 'mst_barang';

    //set names of field
    public $barang_id;
    public $barang_name;
    public $barang_satuan;
    public $user_id;
    public $subgroup_id;
    public $created_at;

    /**
     * Init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('autonum');
        $this->load->library('form_validation');
    }

    /**
     * Make a new model
     * 
     * @return mixed
     */
    public function create()
    {
        $name = $this->input->post('name', true);
        $satuan = $this->input->post('satuan', true);
        $user = 1;
        $subgroup = '01';
        $created_at = strtotime('now');

        $id = generateAutonum('mst_barang', 'barang_id', [
			'key' => 'barang_id',
			'val' => substr($name, 0, 3),
			'place' => 'after'
        ], 2, 2);
        
        $this->barang_id = $id;
        $this->barang_name = $name;        
        $this->barang_satuan = $satuan;
        $this->user_id = $user;
        $this->subgroup_id = $subgroup;
        $this->created_at = $created_at;

        $create = $this->db->insert($this->_table, $this);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a category model
     * @param int id get from input hidden id
     * @return mixed
     */
    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $satuan = $this->input->post('satuan', true);
        $user = 1;
        $subgroup = 1;
        
        $this->barang_name = $name;        
        $this->barang_satuan = $satuan;
        $this->user_id = $user;
        $this->subgroup_id = $subgroup;

        $update = $this->db->update($this->_table, [
            'barang_name' => $this->barang_name,
            'barang_satuan' => $this->barang_satuan,
            'user_id' => $this->user_id,
            'subgroup_id' => $this->subgroup_id
        ], [
            'barang_id' => $id
        ]);

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a category model
     * @param int id
     * @return mixed
     */
    public function delete($id)
    {
        $delete = $this->db->delete($this->_table, ['barang_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Show all model
     */
    public function findAll()
    {
        return $this->db->get($this->_table)->result_array();
    }

    /**
     * Show a model by id
     * @param id
     * @return array
     */
    public function findOne($id)
    {
        return $this->db->get_where($this->_table, ['barang_id' => $id])->row_array();
    }
}
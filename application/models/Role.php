<?php defined('BASEPATH') or exit('No direct script access allowed');

class role extends CI_Model
{
    //set table name
    private static $_table = 'mst_role';
    private static $db;

    //set names of field
    public $role_id;
    public $role_name;
    public $role_status;

    /**
     * Init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        self::$db = &get_instance()->db;
    }

    /**
     * Make a new model
     * 
     * @return mixed
     */
    public function create()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $status = $this->input->post('status', true);

        $this->role_status = $status;
        $this->role_id = $id;
        $this->role_name = $name;        

        $create = $this->db->insert(self::$_table, $this);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a category model
     * @param int id get from input hidden id
     * @return mixed
     */
    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $status = $this->input->post('status', true);

        $this->role_status = $status;
        $this->role_name = $name;

        $update = $this->db->update(self::$_table, [
            'role_name' => $this->role_name,
            'role_status' => $this->role_status,
        ], [
            'role_id' => $id
        ]);

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a category model
     * @param int id
     * @return mixed
     */
    public function delete($id)
    {
        $delete = $this->db->delete(self::$_table, ['role_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Show all model
     */
    public static function findAll()
    {
        return self::$db->get(self::$_table)->result_array();
    }

    /**
     * Show a model by id
     * @param id
     * @return array
     */
    public static function findOne($id)
    {
        return self::$db->get_where(self::$_table, ['role_id' => $id])->row_array();
    }
}
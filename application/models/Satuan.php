<?php defined('BASEPATH') or exit('No direct script access allowed');

class Satuan extends CI_Model
{
    //set table name
    private static $_table = 'mst_satuan';
    private static $db;

    //set names of field
    public $satuan_id;
    public $satuan_name;
    public $satuan_sym;

    /**
     * Init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        self::$db = &get_instance()->db;
    }

        public function rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'Nama',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'sym',
                'label' => 'Simbol',
                'rules' => 'trim|required',
            ],
        ];
    }

    public function validate()
    {
        $validate = $this->form_validation;
        $validate->set_rules($this->rules());

        if ($validate->run()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Make a new model
     * 
     * @return mixed
     */
    public function create()
    {
        $name = $this->input->post('name', true);
        $sym = $this->input->post('sym', true);

        $this->satuan_sym = $sym;
        $this->satuan_name = $name;        

        $create = $this->db->insert(self::$_table, $this);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a category model
     * @param int id get from input hidden id
     * @return mixed
     */
    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $sym = $this->input->post('sym', true);

        $this->satuan_name = $name;
        $this->satuan_sym = $sym;

        $update = $this->db->update(self::$_table, [
            'satuan_name' => $this->satuan_name,
            'satuan_sym' => $this->satuan_sym,
        ], [
            'satuan_id' => $id
        ]);

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a category model
     * @param int id
     * @return mixed
     */
    public function delete($id)
    {
        $delete = $this->db->delete(self::$_table, ['satuan_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Show all model
     */
    public static function findAll()
    {
        return self::$db->get(self::$_table)->result_array();
    }

    /**
     * Show a model by id
     * @param id
     * @return array
     */
    public static function findOne($id)
    {
        return self::$db->get_where(self::$_table, ['satuan_id' => $id])->row_array();
    }
}
<?php defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends CI_Model
{
    //set table name
    private static $_table = 'mst_supplier';
    private static $db;

    //set names of field
    public $supplier_id;
    public $supplier_name;
    public $supplier_address;
    public $supplier_telp;

    /**
     * Init
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        self::$db = &get_instance()->db;
    }

        public function rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'Supplier',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'address',
                'label' => 'Alamat',
                'rules' => 'trim|required',
            ],

            [
                'field' => 'telp',
                'label' => 'Telepon',
                'rules' => 'trim|required',
            ],
        ];
    }

    public function validate()
    {
        $validate = $this->form_validation;
        $validate->set_rules($this->rules());

        if ($validate->run()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Make a new model
     * 
     * @return mixed
     */
    public function create()
    {
        $name = $this->input->post('name', true);
        $address = $this->input->post('address', true);
        $telp = $this->input->post('telp', true);

        
        $this->supplier_address = $address;
        $this->supplier_telp = $telp;
        $this->supplier_name = $name;        

        $create = $this->db->insert(self::$_table, $this);

        if ($create) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a category model
     * @param int id get from input hidden id
     * @return mixed
     */
    public function update()
    {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $address = $this->input->post('address', true);
        $telp = $this->input->post('telp', true);

        $this->supplier_name = $name;
        $this->supplier_address = $address;
        $this->supplier_telp = $telp;

        $update = $this->db->update(self::$_table, [
            'supplier_name' => $this->supplier_name,
            'supplier_address' => $this->supplier_address,
            'supplier_telp' => $this->supplier_telp,
        ], [
            'supplier_id' => $id
        ]);

        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a category model
     * @param int id
     * @return mixed
     */
    public function delete($id)
    {
        $delete = $this->db->delete(self::$_table, ['supplier_id' => $id]);

        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Show all model
     */
    public static function findAll()
    {
        return self::$db->get(self::$_table)->result_array();
    }

    /**
     * Show a model by id
     * @param id
     * @return array
     */
    public static function findOne($id)
    {
        return self::$db->get_where(self::$_table, ['supplier_id' => $id])->row_array();
    }
}
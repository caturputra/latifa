<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <?php echo $this->flash->getFlash() ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a href="<?php echo site_url('supplier/create') ?>" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Supplier</a>
            </h3>
        </div>
        <div class="box-body">
            <table class="table table-hover table-bordered table-striped table-data">
                <thead>
                    <th class="text-center">Aksi</th>
                    <th class="text-center">No.</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Telepon</th>
                    <th class="text-center">Alamat</th>
                </thead>
                <tbody>
                    <?php $inc = 1;
                    foreach ($models as $key => $model) : ?>
                    <tr id="<?php echo $model['supplier_id'] ?>">
                        <td class="text-center" style="width:10em">
                            <a href="<?php echo site_url('supplier/update/' . $model['supplier_id']) ?>" class="btn btn-warning btn-flat"><i class="fa fa-pencil"></i></a>
                            <a href="<?php echo site_url('supplier/delete/' . $model['supplier_id']) ?>" class="btn btn-danger btn-flat" onclick="return confirm('Konfirmasi hapus data!')"><i class="fa fa-trash"></i></a>
                        </td>
                        <td class="text-capitalize text-center"><?php echo $inc++ ?></td>
                        <td class="text-capitalize text-center"><?php echo $model['supplier_name'] ?></td>
                        <td class="text-capitalize text-center"><?php echo $model['supplier_telp'] ?></td>
                        <td class="text-capitalize text-left"><?php echo $model['supplier_address'] ?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content --> 
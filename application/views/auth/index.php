<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <?php echo $this->flash->getFlash() ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a href="<?php echo site_url('user/create') ?>" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Pengguna</a>
            </h3>
        </div>
        <div class="box-body">
            <table class="table table-hover table-bordered table-striped">
                <thead>
					<th class="text-center">No</th>
					<th class="text-center"><?php echo lang('index_action_th');?></th>
					<th class="text-center"><?php echo lang('index_fname_th');?></th>
					<th class="text-center"><?php echo lang('index_lname_th');?></th>
					<th class="text-center"><?php echo lang('index_email_th');?></th>
					<th class="text-center"><?php echo lang('index_groups_th');?></th>
					<th class="text-center"><?php echo lang('index_status_th');?></th>
                </thead>
                <tbody>
					<?php $inc = 1;
                    foreach ($users as $key => $user) : ?>
                    <tr id="<?php echo $user->id ?>">
						<td class="text-capitalize text-center"><?php echo $inc++ ?></td>
                        <td class="text-center" style="width:10em">
                            <a href="<?php echo site_url('user/update/' . $user->id) ?>" class="btn btn-warning btn-flat"><i class="fa fa-pencil"></i></a>
                        </td>
						<td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
						<td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
						<td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
							<?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("user/deactivate/".$user->id, lang('index_active_link')) : anchor("user/activate/". $user->id, lang('index_inactive_link'));?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content --> 
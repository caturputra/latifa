<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo base_url('barang') ?>"><i class="fa fa-database"></i> Data barang</a></li>
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <span>Pembelian</span>
        </div>
        <div class="box-body">
            <div class="col-sm-8">
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                        <span>Input barang</span>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal" action="<?php echo site_url('pembelian/barang') ?>" method="post" id="formBarang">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="barangId" class="control-label col-sm-4">Kode barang</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Pencarian barang..." name="barang_id" id="barangId" readonly>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="btnBarangPembelian" data-toggle="modal" data-target="#modalBarangPembelian"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="expDate" class="control-label col-sm-4">Tgl E.D.</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="exp_date" id="expDate">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label col-sm-4">Tanggal</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="date" id="date">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="condition" class="control-label col-sm-4">Kondisi</label>
                                    <div class="col-sm-8">
                                        <select name="condition" id="condition" class="form-control">
                                            <?php foreach ($conditions as $ket => $val) : ?>
                                                <option value="<?php echo $val['condition_id'] ?>"><?php echo $val['condition_name'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="" class="control-label col-sm-4">Hrg. Satuan</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" name="harga_satuan" id="hargaSatuan">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label col-sm-4">Hrg. Jual</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" name="harga_jual" id="hargaJual">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label col-sm-4">Jumlah</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" name="jumlah" id="jumlah">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="supplier" class="control-label col-sm-4">Supplier</label>
                                    <div class="col-sm-8">
                                    <select name="supplier" id="supplier" class="form-control">
                                            <?php foreach($suppliers as $key => $value) : ?>
                                                <option value="<?php echo $value['supplier_id'] ?>"><?php echo $value['supplier_name'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="pull-right">
                                    <button type="submit" class="btn btn-flat btn-primary btn-sm" id="btnBarangInput"><i class="fa fa-plus-circle"></i> Tambah</button>
                                </div>
                                <br><br>
                            </div>
                        </form>
                        
                        <table class="table table-bordered table-hover table-striped table-responsive table-condensed">
                            <thead>
                                <th>Tanggal Beli</th>
                                <th>Kode Barang</th>
                                <th>E.D</th>
                                <th>Harga satuan</th>
                                <th>Harga jual</th>
                                <th>Jumlah</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <?php foreach($barangs as $key => $barang) : ?>
                                    <tr>
                                        <td><?php echo date('Y-m-d', $barang['tanggal']) ?></td>
                                        <td><?php echo $barang['barang_id'] ?></td>
                                        <td><?php echo date('Y-m-d', $barang['barang_expdate']) ?></td>
                                        <td><?php echo $barang['barang_hargasatuan'] ?></td>
                                        <td><?php echo $barang['barang_hargajual'] ?></td>
                                        <td><?php echo $barang['psd_debet'] ?></td>
                                        <td></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <form class="form-horizontal" method="post" action="<?php echo site_url('pembelian/create') ?>" id="formPembelian">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="" class="control-label col-sm-6">Nomor dokumen</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="no_dokumen" id="fakturPembelian" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-6">Tahun Anggaran</label>
                        <div class="col-sm-6">
                        <select name="tahun_anggaran" id="tahunAnggaran" class="form-control" required>
                            <?php 
                                $year = date('Y', mktime(0, 0, 0, 0, 12-3650));
                                $year_now = date('Y'); 
                                $selected = '';
                            ?>
                            <?php for ($y = $year; $y <= $year_now; $y++) : ?>
                                <?php if ($y == $year_now) : ?>
                                    <?php $selected = 'selected' ?>
                                <?php endif ?>
                                <option value="<?php echo trim($y) ?>" <?php echo $selected ?>><?php echo $y ?></option>
                            <?php endfor ?>
                        </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-6">Sumber dana</label>
                        <div class="col-sm-6">
                            <select name="dana" id="dana" class="form-control">
                                <?php foreach($danas as $key => $value) : ?>
                                    <option value="<?php echo $value['dana_id'] ?>"><?php echo $value['dana_name'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-6">Kelompok</label>
                        <div class="col-sm-6">
                        <select name="group" id="group" class="form-control">
                                <?php foreach($groups as $key => $value) : ?>
                                    <option value="<?php echo $value['group_id'] ?>"><?php echo $value['group_name'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-6">Sub kelompok</label>
                        <div class="col-sm-6">
                            <select name="subgroup" id="subgroup" class="form-control"></select>
                        </div>
                    </div>

                    <div class="pull-right">
                        <button type="submit" class="btn btn-flat btn-primary btn-sm" id="btnPembelianInput"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="modalBarangPembelian" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pilih Barang</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover" id="tableBarangPembelian">
                    <thead>
                        <th class="text-center">Kode Barang</th>
                        <th class="text-center">Nama Barang</th>
                        <th class="text-center">Satuan</th>
                        <th class="text-center">Kelompok</th>
                        <th class="text-center">Sub Kelompok</th>
                    </thead>

                    <tbody>
                        <?php foreach($shows_barang as $key => $val) : ?>
                        <tr>
                            <td style="width: 5%" class="text-center"><?php echo $val['barang_id'] ?></td>
                            <td style="width: 5%" class="text-capitalize"><?php echo $val['barang_name'] ?></td>
                            <td style="width: 5%" class="text-capitalize"><?php echo $val['satuan_name'] ?></td>
                            <td style="width: 5%" class="text-capitalize"><?php echo $val['group_name'] ?></td>
                            <td style="width: 5%" class="text-capitalize"><?php echo $val['subgroup_name'] ?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


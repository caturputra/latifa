<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo base_url('barang') ?>"><i class="fa fa-database"></i> Data barang</a></li>
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>

<section class="content col-sm-9">
    <div class="col-sm-offset-4">
        <div class="box box-warning">
            <div class="box-header with-border">
                <?php echo anchor(base_url('barang/index'), 'Kembali', ['class' => 'btn btn-default btn-flat']) ?>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <?php if ($this->session->flashdata('success')) : ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col-sm-12">
                        <?php echo form_open('barang/update/' . $barang['barang_id'], ['method' => 'post']) ?>
                        <input type="hidden" name="id" value="<?php echo $barang['barang_id'] ?>">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="form-group <?php echo form_error('name') ? 'has-error' : ''; ?>">
                                    <label for="name" class="control-label">Nama barang</label>
                                    <input type="text" class="form-control" name="name" id="name" autofocus value="<?php echo $barang['barang_name'] ?>">
                                    <span class="help-block"><?php echo form_error('name') ?></span>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group <?php echo form_error('satuan') ? 'has-error' : ''; ?>">
                                    <label for="satuan" class="control-label">Satuan</label>
                                    <select class="form-control" name="satuan" id="satuan">
                                    <?php foreach($satuans as $key => $satuan) : ?>
                                        <?php $selected = ''; ?>
                                        <?php 
                                            if ($satuan['satuan_id'] == $barang['barang_satuan']) : 
                                                $selected = 'selected';
                                            endif
                                        ?>
                                        <option value="<?php echo $satuan['satuan_id'] ?>"  <?php echo $selected?>><?php echo $satuan['satuan_name'] ?></option>
                                    <?php endforeach ?>
                                    </select>
                                    <span class="help-block"><?php echo form_error('satuan') ?></span>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group <?php echo form_error('group') ? 'has-error' : ''; ?>">
                                    <label for="group" class="control-label">Kelompok</label>
                                    <select name="group" id="group" class="form-control">
                                        <?php foreach($groups as $key => $value) : ?>
                                            <option value="<?php echo $value['group_id'] ?>"><?php echo $value['group_name'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group <?php echo form_error('subgroup') ? 'has-error' : ''; ?>">
                                    <label for="subgroup" class="control-label">Sub kelompok</label>
                                    <select name="subgroup" id="subgroup" class="form-control"></select>
                                    <span class="help-block"><?php echo form_error('subgroup') ?></span>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-flat btn-primary"><i class="fa fa-save"></i> Simpan</button>
                            <button type="reset" class="btn btn-flat btn-default"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section> 
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <?php echo $this->flash->getFlash() ?>

    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading">Kategori Laporan</div>
            <div class="panel-body">
                <p>...</p>
                </div>
        
                <!-- List group -->
                <ul class="list-group">
                    <li class="list-group-item">Kartu kendali stok per barang label<a href="<?php echo site_url('report/?q=1') ?>" class="btn btn-primary pull-right btn-xs"> Set laporan</a> </li>
                    <li class="list-group-item">Laporan pembelian label<a href="<?php echo site_url('report/?q=2') ?>" class="btn btn-primary pull-right btn-xs"> Set laporan</a></li>
                    <li class="list-group-item">Laporan penjualan label<a href="<?php echo site_url('report/?q=3') ?>" class="btn btn-primary pull-right btn-xs"> Set laporan</a></li>
                    <li class="list-group-item">Laporan total stok label<a href="<?php echo site_url('report/?q=4') ?>" class="btn btn-primary pull-right btn-xs"> Set laporan</a></li>
                </ul>
            </div>
        </div>

        <div class="col-sm-8">
            <?php if (isset($_GET['q'])) : ?>
                <?php switch($_GET['q']): case '1': ?>
                    <?php include_once '1.php' ?>
                <?php break; case '2' : ?>
                    <?php include_once '2.php' ?>
                <?php break; case '3' : ?>
                    <?php include_once '3.php' ?>
                <?php break; case '4' : ?>
                    <?php include_once '4.php' ?>
                <?php break; endswitch; ?>
            <?php else : ?>
                <?php include_once '1.php' ?>
            <?php endif ?>
        </div>
    </div>
</section>
<!-- /.content --> 
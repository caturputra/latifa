<div class="box box-warning">
    <div class="box-body">
        <form action="<?php echo site_url('report/generate/1') ?>" method="post">
            <div class="form-group">
                <label for="barangIdCard" class="control-label col-sm-4">Kode barang</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Pencarian barang..." name="barang_id" id="barangIdCard" readonly>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="btnBarangCard" data-toggle="modal" data-target="#modalCard"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success"> Generate</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal -->
<div id="modalCard" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pilih Barang</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover table-data" id="tableBarangCard">
                    <thead>
                        <th class="text-center">Kode Barang</th>
                        <th class="text-center">Nama Barang</th>
                        <th class="text-center">Satuan</th>
                        <th class="text-center">Kelompok</th>
                        <th class="text-center">Sub Kelompok</th>
                    </thead>

                    <tbody>
                        <?php foreach($shows_barang as $key => $val) : ?>
                        <tr>
                            <td style="width: 5%" class="text-center"><?php echo $val['barang_id'] ?></td>
                            <td style="width: 5%" class="text-capitalize"><?php echo $val['barang_name'] ?></td>
                            <td style="width: 5%" class="text-capitalize"><?php echo $val['satuan_name'] ?></td>
                            <td style="width: 5%" class="text-capitalize"><?php echo $val['group_name'] ?></td>
                            <td style="width: 5%" class="text-capitalize"><?php echo $val['subgroup_name'] ?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<?php include_once "header.php" ?>
<?php include_once "nav.php" ?>
<!-- Full Width Column -->
<div class="content-wrapper">
    <div class="container">
        <?php echo $content ?>
    </div>
    <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<?php include_once "footer.php" ?> 
<footer class="main-footer">
    <div class="container">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.1 <label class="label label-warning"> Alpha </label>
        </div>
        <strong>Copyright &copy; <?php echo date('Y') ?> <a href="#">RSU Queen Latifa</a>.</strong><br>
        <small><strong>Supported by <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.</small>
    </div>
    <!-- /.container -->
</footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/adminlte') ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/adminlte') ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/adminlte') ?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/adminlte') ?>/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/adminlte') ?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/adminlte') ?>/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url('assets/node_modules/datatables.net/js/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/js/latifa.js') ?>"></script>

</body>
</html> 
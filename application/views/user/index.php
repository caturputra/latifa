<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <?php echo $this->flash->getFlash() ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <a href="<?php echo site_url('user/create') ?>" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> User</a>
            </h3>
        </div>
        <div class="box-body">
            <table class="table table-hover table-bordered table-striped table-data">
                <thead>
                    <th class="text-center">Aksi</th>
                    <th class="text-center">No.</th>
                    <th class="text-center">ID</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Unit</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Contact</th>
                    <th class="text-center">Gender</th>
                    <th class="text-center">Level</th>
                    <th class="text-center">Status</th>
                </thead>
                <tbody>
                    <?php $inc = 1;
                    foreach ($models as $key => $model) : ?>
                    <tr id="<?php echo $model['user_id'] ?>">
                        <td class="text-center" style="width:10em">
                                <a href="<?php echo site_url('auth/reset-password/' . $model['user_id']) ?>" class="btn btn-primary bg-purple btn-flat"><i class="fa fa-key"></i></a>
                            <a href="<?php echo site_url('user/update/' . $model['user_id']) ?>" class="btn btn-warning btn-flat"><i class="fa fa-pencil"></i></a>
                            <a href="<?php echo site_url('user/delete/' . $model['user_id']) ?>" class="btn btn-danger btn-flat" onclick="return confirm('Konfirmasi hapus data!')"><i class="fa fa-trash"></i></a>
                        </td>
                        <td class="text-capitalize text-center"><?php echo $inc++ ?></td>
                        <td class="text-capitalize text-center"><?php echo $model['user_id'] ?></td>
                        <td class="text-capitalize text-center"><?php echo $model['user_fullname'] ?></td>
                        <td class="text-capitalize text-center"><?php echo $model['unit_name'] ?></td>
                        <td class="text-capitalize text-center"><?php echo $model['user_email'] ?></td>
                        <td class="text-capitalize text-center"><?php echo $model['user_contact'] ?></td>
                        <td class="text-capitalize text-center">
                            <?php if ($model['user_gender'] == '1') : ?>
                                <span class="label label-default">Laki-laki</span>
                            <?php else: ?>
                                <span class="label label-warning">Perempuan</span>
                            <?php endif ?>
                        </td>
                        <td class="text-capitalize text-center"><?php echo $model['role_name'] ?></td>
                        <td class="text-capitalize text-center">
                            <?php if ($model['user_status'] == '1') : ?>
                                <a href="<?php echo site_url('user/activate/' . $model['user_id']) ?>"><span class="label label-primary">Aktif</span></a>
                            <?php else: ?>
                                <a href="<?php echo site_url('user/activate/' . $model['user_id']) ?>"><span class="label label-info">Nonaktif</span></a>
                            <?php endif ?>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content --> 
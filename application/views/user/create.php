<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo base_url('user') ?>"><i class="fa fa-database"></i> Data user</a></li>
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>

<section class="content">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <?php echo anchor(base_url('user/index'), 'Kembali', ['class' => 'btn btn-default btn-flat']) ?>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <?php if ($this->session->flashdata('success')) : ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>

                <?php echo form_open('user/create', ['method' => 'post']) ?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group <?php echo form_error('id') ? 'has-error' : ''; ?>">
                            <label for="id" class="control-label col-sm-2">ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="id" id="id" autofocus value="<?php echo set_value('id') ?>">
                                <span class="help-block"><?php echo form_error('id') ?></span>
                            </div>
                        </div>

                        <div class="form-group <?php echo form_error('name') ? 'has-error' : ''; ?>">
                            <label for="name" class="control-label col-sm-2">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name') ?>">
                                <span class="help-block"><?php echo form_error('name') ?></span>
                            </div>
                        </div>

                        <div class="form-group <?php echo form_error('email') ? 'has-error' : ''; ?>">
                            <label for="email" class="control-label col-sm-2">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="email" id="email" value="<?php echo set_value('email') ?>">
                                <span class="help-block"><?php echo form_error('email') ?></span>
                            </div>
                        </div>

                        <div class="form-group <?php echo form_error('address') ? 'has-error' : ''; ?>">
                            <label for="address" class="control-label col-sm-2">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="address" id="address" value="<?php echo set_value('address') ?>">
                                <span class="help-block"><?php echo form_error('address') ?></span>
                            </div>
                        </div>

                        <div class="form-group <?php echo form_error('contact') ? 'has-error' : ''; ?>">
                            <label for="contact" class="control-label col-sm-2">Kontak</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="contact" id="contact" value="<?php echo set_value('contact') ?>">
                                <span class="help-block"><?php echo form_error('contact') ?></span>
                            </div>
                        </div>

                        <div class="form-group <?php echo form_error('gender') ? 'has-error' : ''; ?>">
                            <label for="gender" class="control-label col-sm-2">Gender</label>
                            <div class="col-sm-10">
                                <select name="gender" id="unit" class="form-control">
                                    <option value="1">Laki-laki</option>
                                    <option value="2">Laki-Perempuan</option>
                                </select>
                                <span class="help-block"><?php echo form_error('gender') ?></span>
                            </div>
                        </div>

                        <div class="form-group <?php echo form_error('unit') ? 'has-error' : ''; ?>">
                            <label for="unit" class="control-label col-sm-2">Unit</label>
                            <div class="col-sm-10">
                                <select name="unit" id="unit" class="form-control">
                                    <?php foreach($units as $key => $unit) : ?>
                                    <option value="<?php echo $unit['unit_id'] ?>"><?php echo $unit['unit_name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"><?php echo form_error('unit') ?></span>
                            </div>
                        </div>

                        <div class="form-group <?php echo form_error('role') ? 'has-error' : ''; ?>">
                            <label for="role" class="control-label col-sm-2">Role</label>
                            <div class="col-sm-10">
                                <select name="role" id="role" class="form-control">
                                    <?php foreach($roles as $key => $role) : ?>
                                    <option value="<?php echo $role['role_id'] ?>"><?php echo $role['role_name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"><?php echo form_error('unit') ?></span>
                            </div>
                        </div>

                        <div class="form-group <?php echo form_error('status') ? 'has-error' : ''; ?>">
                            <label for="status" class="control-label col-sm-2">Status</label>
                            <div class="col-sm-10">
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Aktif</option>
                                    <option value="2">Nonaktif</option>
                                </select>
                                <span class="help-block"><?php echo form_error('status') ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                User Credential
                            </div>
                            <div class="box-body">
                                <div class="form-group <?php echo form_error('password') ? 'has-error' : ''; ?>">
                                    <label for="password" class="control-label col-sm-4">Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="password" id="password">
                                        <span class="help-block"><?php echo form_error('password') ?></span>
                                    </div>
                                </div>

                                <div class="form-group <?php echo form_error('repeat_password') ? 'has-error' : ''; ?>">
                                    <label for="repeat_password" class="control-label col-sm-4">Re-password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="repeat_password" id="repeat_password">
                                        <span class="help-block"><?php echo form_error('repeat_password') ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-flat btn-primary"><i class="fa fa-save"></i> Simpan</button>
                            <button type="reset" class="btn btn-flat btn-default"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section> 
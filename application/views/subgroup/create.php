<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo base_url('subgroup') ?>"><i class="fa fa-database"></i> Data subgroup</a></li>
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>

<section class="content col-sm-9">
    <div class="col-sm-offset-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <?php echo anchor(base_url('subgroup/index'), 'Kembali', ['class' => 'btn btn-default btn-flat']) ?>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <?php if ($this->session->flashdata('success')) : ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col-sm-12">
                        <?php echo form_open('group/'. $group_id .'/subgroup/create', ['method' => 'post']) ?>

                        <input type="hidden" name="group" value="<?php echo $group_id ?>">
                        <div class="form-group <?php echo form_error('name') ? 'has-error' : ''; ?>">
                            <label for="name" class="control-label">Nama</label>
                            <input type="text" class="form-control" name="name" id="name">
                            <span class="help-block"><?php echo form_error('name') ?></span>
                        </div>

                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-flat btn-primary"><i class="fa fa-save"></i> Simpan</button>
                            <button type="reset" class="btn btn-flat btn-default"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section> 
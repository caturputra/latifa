<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo isset($title) ? ucfirst($title) : '<br>' ?>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><?php echo isset($title) ? ucfirst($title) : '' ?></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <?php echo $this->flash->getFlash() ?>
</section>
<!-- /.content --> 
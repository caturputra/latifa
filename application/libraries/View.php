<?php

/**
 * Library for Dynamic page
 */
class View 
{
    var $template_data = [];

    public function set($name, $value)
    {
        $this->template_data[$name] = $value;        
    }
    
    public function load($view = '', $view_data = '', $template = 'templates/main', $return = false)
    {
        $this->CI = &get_instance();
        $this->set('content', $this->CI->load->view($view, $view_data, true));
        return $this->CI->load->view($template, $this->template_data, $return);
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth
{
    protected $CI;

    public $user = null;
    public $user_id = null;
    public $password = null;
    public $roles = 0;  // [ public $roles = null ] codeIgniter where_in() omitted for null.
    public $is_loggedin = false;
    public $error = array();

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->init();
    }

    /**
     * Initialization the Auth class
     */
    protected function init()
    {
        if ($this->CI->session->has_userdata("user_id") && $this->CI->session->is_loggedin) {
            $this->user_id = $this->CI->session->user_id;
            $this->roles = $this->CI->session->roles;
            $this->is_loggedin = true;
        }

        return;
    }

    /**
     * Handle Login
     *
     * @param $request
     * @return array|bool|void
     */
    public function login()
    {
        if ($this->validate()) {
            $this->user = $this->credentials($this->username, $this->password);
            if ($this->user) {
                return $this->setUser();
            } else {
                return $this->failedLogin();
            }
        }

        return false;
    }

    /**
     * Validate the login form
     *
     * @param $request
     * @return bool
     */
    protected function validate()
    {
        $this->CI->form_validation->set_rules('username', 'User Name', 'trim|required');
        $this->CI->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->CI->form_validation->run() == TRUE) {
            /*$this->username = $request["username"];
            $this->password = $request["password"];*/
            $this->username = $this->CI->input->post("username", TRUE);
            $this->password = $this->CI->input->post("password", TRUE);
            return true;
        }

        return false;
    }

    /**
     * Check the credentials
     *
     * @param $username
     * @param $password
     * @return mixed
     */
    protected function credentials($user_id, $password)
    {
        $user = $this->CI->db->get_where("mst_users", array("user_id" => $user_id, "user_status" => 1))->row(0);
        if($user && password_verify($password, $user->user_password)) {
            return $user;
        }

        return false;
    }

    /**
     * Setting session for authenticated user
     */
    protected function setUser()
    {
        $this->user_id = $this->user->user_id;

        $this->CI->session->set_userdata(array(
            "user_id" => $this->user->user_id,
            "roles" => $this->userWiseRoles(),
            "is_loggedin" => true,
            'user_unit' => $this->userUnit($this->user->user_unit),
            'unit_id' => $this->user->user_unit,
        ));

        return redirect("dashboard");
    }

    /**
     * Get the error message for failed login
     *
     * @param $request
     * @return array
     */
    protected function failedLogin()
    {
        $this->error["failed"] = "username or Password Incorrect.";

        return $this->error;
    }

    /**
     * Check login status
     *
     * @return bool
     */
    public function is_loggedin()
    {
        return $this->is_loggedin;
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function authenticate()
    {
        if (!$this->is_loggedin()) {
            return redirect('login');
        }

        return true;
    }

    /**
     * Determine if the current user is authenticated. Identical of authenticate()
     *
     * @return bool
     */
    public function check($methods = 0)
    {
        if (is_array($methods) && count(is_array($methods))) {
            foreach ($methods as $method) {
                if ($method == (is_null($this->CI->uri->segment(2)) ? "index" : $this->CI->uri->segment(2))) {
                    return $this->authenticate();
                }
            }
        }
        return $this->authenticate();
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return !$this->is_loggedin();
    }

    /**
     * Read authenticated user ID
     *
     * @return int
     */
    public function user_id()
    {
        return $this->user_id;
    }

    /**
     * Read authenticated user roles
     *
     * @return array
     */
    public function roles()
    {
        return $this->roles;
    }

    /**
     * Read the current user roles ID
     *
     * @param $user_id
     * @return string
     */
    protected function userWiseRoles()
    {
        return array_map(function ($item) {
            return $item["role_id"];
        }, $this->CI->db->get_where("role_users", array("user_id" => $this->user_id()))->result_array());
    }

    /**
     * Read the current user roles name
     *
     * @return array
     */
    public function userRoles()
    {
        return array_map(function ($item) {
            return $item["role_name"];
        }, $this->CI->db
            ->select("r.*")
            ->from("mst_role r")
            ->join("role_users ru", "r.role_id = ru.role_id", "inner")
            ->where(array("ru.user_id" => $this->user_id(),"r.role_status" => 1))
            ->get()->result_array());
    }

    /**
     * Determine if the current user is authenticated for specific methods.
     *
     * @param array $methods
     * @return bool
     */
    public function only($methods = array())
    {
        if (is_array($methods) && count(is_array($methods))) {
            foreach ($methods as $method) {
                if ($method == (is_null($this->CI->uri->segment(2)) ? "index" : $this->CI->uri->segment(2))) {
                    return $this->route_access();
                }
            }
        }

        return true;
    }

    /**
     * Determine if the current user is authenticated except specific methods.
     *
     * @param array $methods
     * @return bool
     */
    public function except($methods = array())
    {
        if (is_array($methods) && count(is_array($methods))) {
            foreach ($methods as $method) {
                if ($method == (is_null($this->CI->uri->segment(2)) ? "index" : $this->CI->uri->segment(2))) {
                    return true;
                }
            }
        }

        return $this->route_access();
    }

    /**
     * Determine if the current user is authenticated to view the route/url
     *
     * @return bool|void
     */
    public function route_access()
    {
        $this->check();

        $routeName = (is_null($this->CI->uri->segment(2)) ? "index" : $this->CI->uri->segment(2)) . "-" . $this->CI->uri->segment(1);

        if ($this->CI->uri->segment(1) == 'dashboard')
            return true;

        if($this->can($routeName))
            return true;

        return redirect('exceptions/custom_404', 'refresh');
    }

    /**
     * Checks if the current user has a role by its name.
     *
     * @param $roles
     * @param bool $requireAll
     * @return bool
     */
    public function hasRole($roles, $requireAll = false)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->checkRole($role) && !$requireAll)
                    return true;
                elseif (!$this->checkRole($role) && $requireAll) {
                    return false;
                }
            }
        }
        else {
            return $this->checkRole($roles);
        }
        // If we've made it this far and $requireAll is FALSE, then NONE of the perms were found
        // If we've made it this far and $requireAll is TRUE, then ALL of the perms were found.
        // Return the value of $requireAll;
        return $requireAll;
    }

    /**
     * Check current user has specific role
     *
     * @param $role
     * @return bool
     */
    public function checkRole($role)
    {
        return in_array($role, $this->userRoles());
    }

    /**
     * Logout
     *
     * @return bool
     */
    public function logout()
    {
        $this->CI->session->unset_userdata(array("user_id", "is_loggedin"));
        $this->CI->session->sess_destroy();

        return true;
    }

    public function userUnit($unit_id)
    {
        $unit = $this->CI->db->get_where("mst_unit", array("unit_id" => $unit_id))->result();
        return $unit[0]->unit_name;
    }
}

<?php

class Flash
{
    private function setContainer($status, $value)
    {
        echo "
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='alert alert-{$status}'>
                        {$value}
                    </div>
                </div>
            </div>
        ";
    }

    public function setFlash($value = '', $status = 'success', $data = 'notification')
    {
        $this->CI = &get_instance();

        $this->CI->session->set_flashdata($data, [
            'status' => $status,
            'message' => $value
        ]);
    }

    public function getFlash($data = 'notification')
    {
        $this->CI = &get_instance();

        if ($flash = $this->CI->session->flashdata($data)) {
            $status = $flash['status'];
            $message = $flash['message'];

            $this->setContainer($status, $message);            
        }
    }
}
<?php

class Navigation
{
    private $CI;

    public function setNavigation($role_id = 1)
    {
        $this->CI = &get_instance();
        $menus = $this->CI->db->select('m.menu_id, m.menu_name, m.menu_url, m.menu_icon')
        ->from('role_menu rm')
        ->join('mst_menu m', 'm.menu_id = rm.menu_id')
        ->join('mst_role r', 'r.role_id = rm.role_id')
        ->where(['rm.role_id' => "{$role_id}", 'm.menu_parent' => '0', 'm.menu_visible' => '1'])
        ->order_by('m.menu_order ASC, m.menu_name ASC')
        ->get();

        $html = '';

        foreach ($menus->result() as $key => $menu) {
            $submenu = $this->CI->db->select('m.menu_id, m.menu_name, m.menu_url, m.menu_icon')
                        ->from('role_menu rm')
                        ->join('mst_menu m', 'm.menu_id = rm.menu_id')
                        ->join('mst_role r', 'r.role_id = rm.role_id')
                        ->where(['r.role_id' => "{$role_id}", 'm.menu_parent' => $menu->menu_id, 'm.menu_visible' => '1'])
                        ->order_by('m.menu_order ASC, m.menu_name ASC')
                        ->get();

            if ($submenu->num_rows() > 0) {
                echo '<li class="dropdown">';
                    echo '<a href="'.site_url($menu->menu_url).'" class="dropdown-toggle" data-toggle="dropdown"><i class="' . $menu->menu_icon. '"></i> ' . $menu->menu_name . ' <span class="caret"></span></a>';
                    echo "<ul class='dropdown-menu'>";
                        foreach ($submenu->result() as $key => $submenu) {
                                    echo '<li><a href="'. site_url($submenu->menu_url) .'" class=" hvr-bounce-to-right"> <i class="'. $submenu->menu_icon .'  nav_icon"></i>'. $submenu->menu_name .'</a></li>';
                        }
                echo"</ul></li>";
            } else {
                echo '<li>
  								<a href="' . site_url($menu->menu_url) .'" class=" hvr-bounce-to-right"><i class="' . $menu->menu_icon . ' nav_icon"></i> <span class="nav-label">' . $menu->menu_name . '</span> </a>
  							</li>
';
            }
        }
        // var_dump($menus);
        // var_dump($this->CI->db->last_query());
        // return $html;
    }
}
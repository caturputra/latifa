<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['report'] = 'reportController/index';
$route['report/index'] = 'reportController/index';

$route['permintaan'] = 'permintaanController/index';
$route['permintaan/index'] = 'permintaanController/index';
$route['permintaan/create'] = 'permintaanController/create';
$route['permintaan/update/(:any)'] = 'permintaanController/update/$1';
$route['permintaan/delete/(:any)'] = 'permintaanController/delete/$1';
$route['permintaan/barang'] = 'permintaanController/setBarang';

$route['pembelian'] = 'pembelianController/index';
$route['pembelian/index'] = 'pembelianController/index';
$route['pembelian/create'] = 'pembelianController/create';
$route['pembelian/update/(:any)'] = 'pembelianController/update/$1';
$route['pembelian/delete/(:any)'] = 'pembelianController/delete/$1';
$route['pembelian/barang'] = 'pembelianController/setBarang';

$route['barang'] = 'barangController/index';
$route['barang/index'] = 'barangController/index';
$route['barang/create'] = 'barangController/create';
$route['barang/update/(:any)'] = 'barangController/update/$1';
$route['barang/delete/(:any)'] = 'barangController/delete/$1';

$route['unit'] = 'unitController/index';
$route['unit/index'] = 'unitController/index';
$route['unit/create'] = 'unitController/create';
$route['unit/update/(:any)'] = 'unitController/update/$1';
$route['unit/delete/(:any)'] = 'unitController/delete/$1';

$route['group'] = 'groupController/index';
$route['group/index'] = 'groupController/index';
$route['group/create'] = 'groupController/create';
$route['group/update/(:any)'] = 'groupController/update/$1';
$route['group/delete/(:any)'] = 'groupController/delete/$1';
$route['group/(:any)/subgroup'] = 'groupController/subgroup/$1';
$route['group/(:any)/subgroup/index'] = 'groupController/subgroup/$1';
$route['group/(:any)/subgroup/create'] = 'groupController/createSubgroup/$1';

$route['supplier'] = 'supplierController/index';
$route['supplier/index'] = 'supplierController/index';
$route['supplier/create'] = 'supplierController/create';
$route['supplier/update/(:any)'] = 'supplierController/update/$1';
$route['supplier/delete/(:any)'] = 'supplierController/delete/$1';

$route['satuan'] = 'satuanController/index';
$route['satuan/index'] = 'satuanController/index';
$route['satuan/create'] = 'satuanController/create';
$route['satuan/update/(:any)'] = 'satuanController/update/$1';
$route['satuan/delete/(:any)'] = 'satuanController/delete/$1';

$route['role'] = 'roleController/index';
$route['role/index'] = 'roleController/index';
$route['role/create'] = 'roleController/create';
$route['role/update/(:any)'] = 'roleController/update/$1';
$route['role/delete/(:any)'] = 'roleController/delete/$1';

$route['dana'] = 'danaController/index';
$route['dana/index'] = 'danaController/index';
$route['dana/create'] = 'danaController/create';
$route['dana/update/(:any)'] = 'danaController/update/$1';
$route['dana/delete/(:any)'] = 'danaController/delete/$1';

$route['condition'] = 'conditionController/index';
$route['condition/index'] = 'conditionController/index';
$route['condition/create'] = 'conditionController/create';
$route['condition/update/(:any)'] = 'conditionController/update/$1';
$route['condition/delete/(:any)'] = 'conditionController/delete/$1';

$route['auth'] = 'authController/index';
$route['auth/index'] = 'authController/index';
$route['auth/signup'] = 'authController/register';
$route['auth/signin'] = 'authController/login';
$route['auth/signout'] = 'authController/logout';
$route['auth/setting'] = 'authController/change_password';
$route['auth/forgot-password'] = 'authController/forgot_password';
$route['auth/reset-password'] = 'authController/reset_password';

$route['user/activate/(:any)'] = 'userController/activate/$1';
$route['user/profile/edit'] = 'userController/updateProfile';
$route['user/profile/edit'] = 'userController/updateProfile';
$route['user/profile'] = 'userController/index';
$route['user'] = 'userController/index';
$route['user/index'] = 'userController/index';
$route['user/create'] = 'userController/create';
$route['user/update/(:any)'] = 'userController/update/$1';
$route['user/delete/(:any)'] = 'userController/delete/$1';

$route['dashboard'] = 'dashboardController/index';
$route['dashboard/index'] = 'dashboardController/index';

$route['ajax/subgroup'] = 'ajaxController/dropdownSubgroup';

// $route['default_controller'] = 'authController/login';
$route['default_controller'] = 'dashboardController/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

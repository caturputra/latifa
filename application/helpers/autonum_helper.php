<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Fungsi untuk generate autonumber
 * @param $table    string  tabel yang dituju
 * @param $key      string  key yang dijadikan acuan
 * @param $where    array   key => kondisi like, val => string yang dicari, place => posisi wildcard                                    (before, after, none, both)
 * @param $lnum     int     angka panjang autonumber
 * @param $lfirst   string  panjang string didepan
 */
function generateAutonum($table, $key, array $where, $lfirst = 3, $lnum = 2)
{
    $ci = &get_instance();
    $ci->db->select('MAX(' . $key . ') as lastnum');
    $ci->db->from($table);
    $ci->db->like($where['key'], $where['val'], $where['place']);

    $result = $ci->db->get();

    if ($result->num_rows() != 0) {
        $row = $result->row();
        if ($row->lastnum != null) {
            $num = substr($row->lastnum, $lfirst + 1, $lnum);
            $num++;
        } else {
            $num = 1;
        }
    } else {
        $num = 1;
    }

    $autonum = strtoupper($where['val']) . sprintf("%0". $lnum ."s", $num);

    return $autonum;
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Contoller extends CI_Controller
{
    protected $scope = '*';

    public function __construct()
    {
        parent::__construct();
        $this->checkPermission();
    }

    public function checkPermission()
    {
        if ($this->scope != '*') {
            if (!$this->auth->is_loggedin()) {
			    // redirect them to the login page
                redirect('auth/signin', 'refresh');
            }
        
            if (!$this->getPermission()) {
                return show_error('You do not have previllage to access this page.', 403, 'Forbidden Access!');
                // return redirect('auth/signin');
            }
        }
    }

    public function getPermission()
    {
        if ($this->scope == '@') {
            return true;
        } else {
            $scope = is_array($this->scope) ? $this->scope : explode(',', $this->scope);
            
            if ($this->auth->hasRole(array_map('trim', $scope))) {
                return true;
            }

            return false;
        }

        return false;
    }
}
?>
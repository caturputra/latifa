-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE `groups`;
INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1,	'admin',	'Administrator'),
(2,	'members',	'General User');

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE `login_attempts`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `mst_barang`;
CREATE TABLE `mst_barang` (
  `barang_id` char(5) NOT NULL,
  `barang_name` varchar(75) NOT NULL,
  `barang_stock` float NOT NULL,
  `barang_hargasatuan` float NOT NULL,
  `barang_hargajual` float NOT NULL,
  `barang_satuan` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `subgroup_id` char(2) NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`barang_id`),
  KEY `user_id` (`user_id`),
  KEY `barang_satuan` (`barang_satuan`),
  KEY `subgroup_id` (`subgroup_id`),
  CONSTRAINT `mst_barang_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `mst_users` (`user_id`),
  CONSTRAINT `mst_barang_ibfk_2` FOREIGN KEY (`barang_satuan`) REFERENCES `mst_satuan` (`satuan_id`),
  CONSTRAINT `mst_barang_ibfk_3` FOREIGN KEY (`subgroup_id`) REFERENCES `mst_subgroup` (`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_barang`;
INSERT INTO `mst_barang` (`barang_id`, `barang_name`, `barang_stock`, `barang_hargasatuan`, `barang_hargajual`, `barang_satuan`, `user_id`, `subgroup_id`, `created_at`) VALUES
('FOL01',	'folio',	0,	0,	0,	1,	'199602152019021005',	'01',	1570457259),
('FOL02',	'folio 80 gr',	0,	0,	0,	1,	'199602152019021005',	'01',	1570457274),
('GEL01',	'Gelas kecil',	0,	5000,	7000,	1,	'199602152019021005',	'01',	1),
('GEL02',	'Gelas kecil',	0,	5000,	7000,	1,	'199602152019021005',	'01',	1),
('GEL03',	'Gelas kecil 2',	0,	5000,	7000,	1,	'199602152019021005',	'01',	1),
('KER01',	'kertas folio',	0,	0,	0,	1,	'199602152019021005',	'01',	1570457248);

DROP TABLE IF EXISTS `mst_condition`;
CREATE TABLE `mst_condition` (
  `condition_id` int(11) NOT NULL AUTO_INCREMENT,
  `condition_name` varchar(32) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_condition`;
INSERT INTO `mst_condition` (`condition_id`, `condition_name`) VALUES
(1,	'Baik'),
(2,	'Rusak');

DROP TABLE IF EXISTS `mst_dana`;
CREATE TABLE `mst_dana` (
  `dana_id` char(2) NOT NULL,
  `dana_name` char(15) NOT NULL,
  PRIMARY KEY (`dana_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_dana`;
INSERT INTO `mst_dana` (`dana_id`, `dana_name`) VALUES
('01',	'BLUD'),
('02',	'APBD');

DROP TABLE IF EXISTS `mst_group`;
CREATE TABLE `mst_group` (
  `group_id` char(2) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_group`;
INSERT INTO `mst_group` (`group_id`, `group_name`) VALUES
('01',	'Habis Pakai'),
('02',	'Tidak habis pakai');

DROP TABLE IF EXISTS `mst_menu`;
CREATE TABLE `mst_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_parent` int(11) NOT NULL DEFAULT 0,
  `menu_name` varchar(50) NOT NULL,
  `menu_icon` varchar(75) NOT NULL,
  `menu_url` text NOT NULL,
  `menu_visible` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_menu`;

DROP TABLE IF EXISTS `mst_role`;
CREATE TABLE `mst_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) NOT NULL,
  `role_status` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_role`;
INSERT INTO `mst_role` (`role_id`, `role_name`, `role_status`) VALUES
(1,	'admin',	'1'),
(2,	'operator',	'1');

DROP TABLE IF EXISTS `mst_satuan`;
CREATE TABLE `mst_satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan_name` varchar(20) NOT NULL,
  `satuan_sym` char(6) NOT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_satuan`;
INSERT INTO `mst_satuan` (`satuan_id`, `satuan_name`, `satuan_sym`) VALUES
(1,	'kilograms',	'kg');

DROP TABLE IF EXISTS `mst_subgroup`;
CREATE TABLE `mst_subgroup` (
  `subgroup_id` char(2) NOT NULL,
  `subgroup_name` varchar(25) NOT NULL,
  `group_id` char(2) NOT NULL,
  PRIMARY KEY (`subgroup_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `mst_subgroup_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `mst_group` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_subgroup`;
INSERT INTO `mst_subgroup` (`subgroup_id`, `subgroup_name`, `group_id`) VALUES
('01',	'Perlatan kantor',	'01'),
('02',	'Obat-obatan',	'01');

DROP TABLE IF EXISTS `mst_supplier`;
CREATE TABLE `mst_supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(75) NOT NULL,
  `supplier_address` mediumtext NOT NULL,
  `supplier_telp` varchar(13) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_supplier`;
INSERT INTO `mst_supplier` (`supplier_id`, `supplier_name`, `supplier_address`, `supplier_telp`) VALUES
(1,	'Sup01',	'akjsdkajsd',	'823794823');

DROP TABLE IF EXISTS `mst_unit`;
CREATE TABLE `mst_unit` (
  `unit_id` char(4) NOT NULL,
  `unit_name` varchar(20) NOT NULL,
  `unit_status` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_unit`;
INSERT INTO `mst_unit` (`unit_id`, `unit_name`, `unit_status`) VALUES
('F001',	'Test',	'1'),
('G001',	'Gudang',	'1');

DROP TABLE IF EXISTS `mst_users`;
CREATE TABLE `mst_users` (
  `user_id` varchar(20) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_fullname` varchar(32) NOT NULL,
  `user_role` int(11) NOT NULL,
  `user_email` mediumtext NOT NULL,
  `user_unit` char(4) NOT NULL,
  `user_gender` char(1) NOT NULL,
  `user_address` mediumtext NOT NULL,
  `user_contact` varchar(12) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_role` (`user_role`),
  KEY `user_unit` (`user_unit`),
  CONSTRAINT `mst_users_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `mst_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mst_users_ibfk_2` FOREIGN KEY (`user_unit`) REFERENCES `mst_unit` (`unit_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_users`;
INSERT INTO `mst_users` (`user_id`, `user_name`, `user_password`, `user_fullname`, `user_role`, `user_email`, `user_unit`, `user_gender`, `user_address`, `user_contact`) VALUES
('1',	'admin',	'admin',	'admin',	1,	'',	'F001',	'1',	'',	''),
('199602152019021005',	'admin',	'admin',	'admin',	1,	'',	'F001',	'1',	'',	'');

DROP TABLE IF EXISTS `pembelian_temp`;
CREATE TABLE `pembelian_temp` (
  `barang_id` char(5) NOT NULL,
  `condition_id` tinyint(4) NOT NULL,
  `barang_expdate` int(11) DEFAULT NULL,
  `barang_hargasatuan` float DEFAULT NULL,
  `barang_hargajual` float DEFAULT NULL,
  `psd_debet` float DEFAULT NULL,
  `tanggal` int(11) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `session_id` varchar(32) DEFAULT NULL,
  `hostname` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `pembelian_temp`;
INSERT INTO `pembelian_temp` (`barang_id`, `condition_id`, `barang_expdate`, `barang_hargasatuan`, `barang_hargajual`, `psd_debet`, `tanggal`, `user_id`, `session_id`, `hostname`) VALUES
('GEL01',	1,	1570467600,	200,	200,	20,	1570467600,	'199602152019021005',	'thp7n6h7cl68critmjunjkgitcclihkp',	'localhost'),
('GEL02',	1,	1570467600,	200,	200,	10,	1570467600,	'199602152019021005',	'thp7n6h7cl68critmjunjkgitcclihkp',	'localhost'),
('GEL03',	1,	1570467600,	200,	500,	200,	1570467600,	'199602152019021005',	'thp7n6h7cl68critmjunjkgitcclihkp',	'localhost');

DROP TABLE IF EXISTS `permintaan_temp`;
CREATE TABLE `permintaan_temp` (
  `barang_id` char(5) NOT NULL,
  `psd_kredit` float NOT NULL,
  `tanggal` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `hostname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `permintaan_temp`;
INSERT INTO `permintaan_temp` (`barang_id`, `psd_kredit`, `tanggal`, `user_id`, `session_id`, `hostname`) VALUES
('GEL01',	10,	1570630676,	'199602152019021005',	'3mc9maq4hfrcth8ura6guj7qvrvg6ukg',	'xexax');

DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `role_menu_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `mst_role` (`role_id`),
  CONSTRAINT `role_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `mst_menu` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `role_menu`;

DROP TABLE IF EXISTS `trx_barang`;
CREATE TABLE `trx_barang` (
  `barang_tahunanggaran` char(4) NOT NULL,
  `unit_id` char(4) NOT NULL,
  `subgroup_id` char(2) NOT NULL,
  `barang_id` char(5) NOT NULL,
  `barang_expdate` int(11) NOT NULL,
  `barang_hargasatuan` float NOT NULL,
  `barang_hargajual` float NOT NULL,
  `dana_id` char(2) NOT NULL,
  `created_at` int(11) NOT NULL,
  `barang_tanggal` date NOT NULL,
  `barang_status` char(1) NOT NULL DEFAULT '1' COMMENT '1: Pembelian, 2:Penjualan',
  KEY `unit_id` (`unit_id`),
  KEY `subgroup_id` (`subgroup_id`),
  KEY `barang_id` (`barang_id`),
  KEY `dana_id` (`dana_id`),
  CONSTRAINT `trx_barang_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `mst_unit` (`unit_id`),
  CONSTRAINT `trx_barang_ibfk_2` FOREIGN KEY (`subgroup_id`) REFERENCES `mst_subgroup` (`subgroup_id`),
  CONSTRAINT `trx_barang_ibfk_3` FOREIGN KEY (`barang_id`) REFERENCES `mst_barang` (`barang_id`),
  CONSTRAINT `trx_barang_ibfk_4` FOREIGN KEY (`dana_id`) REFERENCES `mst_dana` (`dana_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `trx_barang`;
INSERT INTO `trx_barang` (`barang_tahunanggaran`, `unit_id`, `subgroup_id`, `barang_id`, `barang_expdate`, `barang_hargasatuan`, `barang_hargajual`, `dana_id`, `created_at`, `barang_tanggal`, `barang_status`) VALUES
('2019',	'G001',	'01',	'GEL01',	1570467600,	200,	200,	'01',	1570521843,	'0000-00-00',	'1'),
('2019',	'G001',	'01',	'GEL02',	1570467600,	200,	200,	'01',	1570521843,	'0000-00-00',	'1'),
('2019',	'G001',	'01',	'GEL03',	1570467600,	200,	500,	'01',	1570521843,	'0000-00-00',	'1'),
('2019',	'G001',	'01',	'GEL01',	1570467600,	200,	200,	'02',	1570521871,	'0000-00-00',	'1'),
('2019',	'G001',	'01',	'GEL02',	1570467600,	200,	200,	'02',	1570521871,	'0000-00-00',	'1'),
('2019',	'G001',	'01',	'GEL03',	1570467600,	200,	500,	'02',	1570521871,	'0000-00-00',	'1');

DROP TABLE IF EXISTS `trx_persediaan`;
CREATE TABLE `trx_persediaan` (
  `trx_id` char(3) NOT NULL COMMENT '101: Pembelian. 102: Penjualan',
  `barang_id` char(5) NOT NULL,
  `dana_id` char(2) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `psd_tahunperolehan` char(4) NOT NULL,
  `psd_lokasi` varchar(50) NOT NULL,
  `psd_jumlah` int(11) NOT NULL COMMENT 'jumlah stok',
  `psd_debet` float NOT NULL DEFAULT 0,
  `psd_kredit` float NOT NULL DEFAULT 0,
  `psd_harga` float NOT NULL COMMENT 'harga satuan',
  `psd_hargajual` float NOT NULL,
  `psd_d_k` char(1) NOT NULL DEFAULT 'D',
  `psd_nodokumen` varchar(50) NOT NULL,
  `psd_tgldokumen` date NOT NULL,
  `psd_keterangan` mediumtext NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `created_at` int(11) NOT NULL,
  KEY `barang_id` (`barang_id`),
  KEY `dana_id` (`dana_id`),
  KEY `user_id` (`user_id`),
  KEY `suppiler_id` (`supplier_id`),
  KEY `condition_id` (`condition_id`),
  CONSTRAINT `trx_persediaan_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `mst_barang` (`barang_id`),
  CONSTRAINT `trx_persediaan_ibfk_2` FOREIGN KEY (`dana_id`) REFERENCES `mst_dana` (`dana_id`),
  CONSTRAINT `trx_persediaan_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `mst_users` (`user_id`),
  CONSTRAINT `trx_persediaan_ibfk_4` FOREIGN KEY (`supplier_id`) REFERENCES `mst_supplier` (`supplier_id`),
  CONSTRAINT `trx_persediaan_ibfk_5` FOREIGN KEY (`condition_id`) REFERENCES `mst_condition` (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `trx_persediaan`;
INSERT INTO `trx_persediaan` (`trx_id`, `barang_id`, `dana_id`, `condition_id`, `psd_tahunperolehan`, `psd_lokasi`, `psd_jumlah`, `psd_debet`, `psd_kredit`, `psd_harga`, `psd_hargajual`, `psd_d_k`, `psd_nodokumen`, `psd_tgldokumen`, `psd_keterangan`, `supplier_id`, `user_id`, `created_at`) VALUES
('101',	'GEL01',	'01',	1,	'2019',	'G001',	20,	20,	0,	200,	200,	'D',	'20191008.PEM.00001',	'2019-10-08',	'Pembelian dari supplier',	1,	'199602152019021005',	1570521843),
('101',	'GEL02',	'01',	1,	'2019',	'G001',	10,	10,	0,	200,	200,	'D',	'20191008.PEM.00001',	'2019-10-08',	'Pembelian dari supplier',	1,	'199602152019021005',	1570521843),
('101',	'GEL03',	'01',	1,	'2019',	'G001',	200,	200,	0,	200,	500,	'D',	'20191008.PEM.00001',	'2019-10-08',	'Pembelian dari supplier',	1,	'199602152019021005',	1570521843),
('101',	'GEL01',	'02',	1,	'2019',	'G001',	20,	20,	0,	200,	200,	'D',	'20191008.PEM.00002',	'2019-10-08',	'Pembelian dari supplier',	1,	'199602152019021005',	1570521871),
('101',	'GEL02',	'02',	1,	'2019',	'G001',	10,	10,	0,	200,	200,	'D',	'20191008.PEM.00002',	'2019-10-08',	'Pembelian dari supplier',	1,	'199602152019021005',	1570521871),
('101',	'GEL03',	'02',	1,	'2019',	'G001',	200,	200,	0,	200,	500,	'D',	'20191008.PEM.00002',	'2019-10-08',	'Pembelian dari supplier',	1,	'199602152019021005',	1570521871);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_email` (`email`),
  UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  UNIQUE KEY `uc_remember_selector` (`remember_selector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE `users`;
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1,	'127.0.0.1',	'administrator',	'$argon2i$v=19$m=16384,t=4,p=2$eXlsUHhiVHVYcmpZb052VQ$himhwIvP/UcC9lFJXTixhS0azfZEBYxy/Jm3dczeu7I',	'admin@admin.com',	NULL,	'',	NULL,	NULL,	NULL,	NULL,	NULL,	1268889823,	1571359412,	1,	'Admin',	'istrator',	'ADMIN',	'0');

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE `users_groups`;
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(3,	1,	1),
(4,	1,	2);

-- 2019-10-27 02:58:40

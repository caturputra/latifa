-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `latifa`;
CREATE DATABASE `latifa` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `latifa`;

DROP TABLE IF EXISTS `mst_barang`;
CREATE TABLE `mst_barang` (
  `barang_id` char(5) NOT NULL,
  `barang_name` varchar(75) NOT NULL,
  `barang_stock` float NOT NULL,
  `barang_hargasatuan` float NOT NULL,
  `barang_hargajual` float NOT NULL,
  `barang_satuan` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `subgroup_id` char(2) NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`barang_id`),
  KEY `user_id` (`user_id`),
  KEY `barang_satuan` (`barang_satuan`),
  KEY `subgroup_id` (`subgroup_id`),
  CONSTRAINT `mst_barang_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `mst_users` (`user_id`),
  CONSTRAINT `mst_barang_ibfk_2` FOREIGN KEY (`barang_satuan`) REFERENCES `mst_satuan` (`satuan_id`),
  CONSTRAINT `mst_barang_ibfk_3` FOREIGN KEY (`subgroup_id`) REFERENCES `mst_subgroup` (`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_barang`;
INSERT INTO `mst_barang` (`barang_id`, `barang_name`, `barang_stock`, `barang_hargasatuan`, `barang_hargajual`, `barang_satuan`, `user_id`, `subgroup_id`, `created_at`) VALUES
('FOL01',	'Folio 70 Gr',	7,	0,	0,	1,	'199602152019021005',	'01',	1572166951),
('OBA01',	'Obat',	3,	0,	0,	3,	'199602152019021005',	'02',	1572532445);

DROP TABLE IF EXISTS `mst_condition`;
CREATE TABLE `mst_condition` (
  `condition_id` int(11) NOT NULL AUTO_INCREMENT,
  `condition_name` varchar(32) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_condition`;
INSERT INTO `mst_condition` (`condition_id`, `condition_name`) VALUES
(1,	'Baik'),
(2,	'Rusak');

DROP TABLE IF EXISTS `mst_dana`;
CREATE TABLE `mst_dana` (
  `dana_id` char(2) NOT NULL,
  `dana_name` char(15) NOT NULL,
  PRIMARY KEY (`dana_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_dana`;
INSERT INTO `mst_dana` (`dana_id`, `dana_name`) VALUES
('01',	'BLUD'),
('02',	'APBD'),
('03',	'Jadi jadian');

DROP TABLE IF EXISTS `mst_group`;
CREATE TABLE `mst_group` (
  `group_id` char(2) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_group`;
INSERT INTO `mst_group` (`group_id`, `group_name`) VALUES
('01',	'Habis Pakai'),
('02',	'Tidak habis pakai');

DROP TABLE IF EXISTS `mst_menu`;
CREATE TABLE `mst_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_parent` int(11) NOT NULL DEFAULT '0',
  `menu_name` varchar(50) NOT NULL,
  `menu_icon` varchar(75) NOT NULL,
  `menu_url` text NOT NULL,
  `menu_order` tinyint(4) NOT NULL,
  `menu_visible` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_menu`;
INSERT INTO `mst_menu` (`menu_id`, `menu_parent`, `menu_name`, `menu_icon`, `menu_url`, `menu_order`, `menu_visible`) VALUES
(1,	0,	'Dashboard',	'fa fa-home',	'dashboard/index',	0,	'1'),
(2,	0,	'Pembelian',	'fa fa-plane',	'pembelian/create',	2,	'1'),
(3,	0,	'Permintaan',	'fa fa-plane',	'permintaan/create',	3,	'1'),
(4,	0,	'Laporan',	'fa fa-book',	'report/index',	4,	'1'),
(5,	0,	'Master',	'fa fa-database',	'#',	1,	'1'),
(6,	5,	'Barang',	'fa fa-circle-o',	'barang/index',	0,	'1'),
(7,	5,	'Kondisi',	'fa fa-circle-o',	'condition/index',	1,	'1'),
(8,	5,	'Dana',	'fa fa-circle-o',	'dana/index',	2,	'1'),
(9,	5,	'Group',	'fa fa-circle-o',	'group/index',	3,	'1'),
(10,	5,	'Role',	'fa fa-circle-o',	'role/index',	5,	'1'),
(11,	5,	'Satuan',	'fa fa-circle-o',	'satuan/index',	6,	'1'),
(12,	5,	'Supplier',	'fa fa-circle-o',	'supplier/index',	7,	'1'),
(13,	5,	'Unit',	'fa fa-circle-o',	'unit/index',	8,	'1'),
(14,	5,	'Pengguna',	'fa fa-circle-o',	'user/index',	9,	'1'),
(15,	5,	'SubGroup',	'	 fa fa-circle-o',	'subgroup/index',	4,	'1');

DROP TABLE IF EXISTS `mst_role`;
CREATE TABLE `mst_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) NOT NULL,
  `role_status` char(1) NOT NULL DEFAULT '1',
  `role_description` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_role`;
INSERT INTO `mst_role` (`role_id`, `role_name`, `role_status`, `role_description`) VALUES
(1,	'admin',	'1',	'Administrator'),
(2,	'operator',	'1',	'Operator'),
(3,	'kagudang',	'1',	'Kepala Gudang');

DROP TABLE IF EXISTS `mst_satuan`;
CREATE TABLE `mst_satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan_name` varchar(20) NOT NULL,
  `satuan_sym` char(6) NOT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_satuan`;
INSERT INTO `mst_satuan` (`satuan_id`, `satuan_name`, `satuan_sym`) VALUES
(1,	'kilograms',	'kg'),
(2,	'Unit',	'un'),
(3,	'Box',	'box'),
(4,	'pieces',	'pcs');

DROP TABLE IF EXISTS `mst_subgroup`;
CREATE TABLE `mst_subgroup` (
  `subgroup_id` char(2) NOT NULL,
  `subgroup_name` varchar(25) NOT NULL,
  `group_id` char(2) NOT NULL,
  PRIMARY KEY (`subgroup_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `mst_subgroup_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `mst_group` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_subgroup`;
INSERT INTO `mst_subgroup` (`subgroup_id`, `subgroup_name`, `group_id`) VALUES
('01',	'Perlatan kantor',	'01'),
('02',	'Obat-obatan',	'01'),
('03',	'test',	'01'),
('04',	'ahsgdjahsgd',	'01'),
('05',	'jlkjdsflk',	'02');

DROP TABLE IF EXISTS `mst_supplier`;
CREATE TABLE `mst_supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(75) NOT NULL,
  `supplier_address` mediumtext NOT NULL,
  `supplier_telp` varchar(13) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_supplier`;
INSERT INTO `mst_supplier` (`supplier_id`, `supplier_name`, `supplier_address`, `supplier_telp`) VALUES
(3,	'Supplier 1',	'Jawa tengah',	'0');

DROP TABLE IF EXISTS `mst_unit`;
CREATE TABLE `mst_unit` (
  `unit_id` char(4) NOT NULL,
  `unit_name` varchar(20) NOT NULL,
  `unit_status` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_unit`;
INSERT INTO `mst_unit` (`unit_id`, `unit_name`, `unit_status`) VALUES
('F001',	'Gudang Farmasi',	'1'),
('G001',	'Gudang Umum',	'1');

DROP TABLE IF EXISTS `mst_users`;
CREATE TABLE `mst_users` (
  `user_id` varchar(20) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_fullname` varchar(32) NOT NULL,
  `user_role` int(11) NOT NULL,
  `user_email` mediumtext NOT NULL,
  `user_unit` char(4) NOT NULL,
  `user_gender` char(1) NOT NULL,
  `user_address` mediumtext NOT NULL,
  `user_contact` varchar(12) NOT NULL,
  `user_status` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  KEY `user_role` (`user_role`),
  KEY `user_unit` (`user_unit`),
  CONSTRAINT `mst_users_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `mst_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mst_users_ibfk_2` FOREIGN KEY (`user_unit`) REFERENCES `mst_unit` (`unit_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `mst_users`;
INSERT INTO `mst_users` (`user_id`, `user_name`, `user_password`, `user_fullname`, `user_role`, `user_email`, `user_unit`, `user_gender`, `user_address`, `user_contact`, `user_status`) VALUES
('001',	'gudang',	'$2y$10$hKIzUlTQ6D.RkIuMio.0IeiSY48ORg.6WXYi/jX5Vbgipt4TOEI3a',	'gudang',	3,	'gudang@gudang.com',	'G001',	'1',	'gudang',	'0897876',	'1'),
('199602152019021005',	'admin',	'$2y$10$hKIzUlTQ6D.RkIuMio.0IeiSY48ORg.6WXYi/jX5Vbgipt4TOEI3a',	'admin',	1,	'',	'F001',	'1',	'',	'',	'1'),
('199602152019021011',	'asdf',	'$2y$10$hKIzUlTQ6D.RkIuMio.0IeiSY48ORg.6WXYi/jX5Vbgipt4TOEI3a',	'asdf',	2,	'',	'G001',	'1',	'',	'',	'1');

DROP TABLE IF EXISTS `pembelian_temp`;
CREATE TABLE `pembelian_temp` (
  `barang_id` char(5) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `condition_id` tinyint(4) NOT NULL,
  `barang_expdate` int(11) DEFAULT NULL,
  `barang_hargasatuan` float DEFAULT NULL,
  `barang_hargajual` float DEFAULT NULL,
  `psd_debet` float DEFAULT NULL,
  `tanggal` int(11) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `session_id` varchar(32) DEFAULT NULL,
  `hostname` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `pembelian_temp`;
INSERT INTO `pembelian_temp` (`barang_id`, `supplier_id`, `condition_id`, `barang_expdate`, `barang_hargasatuan`, `barang_hargajual`, `psd_debet`, `tanggal`, `user_id`, `session_id`, `hostname`) VALUES
('',	3,	1,	0,	0,	0,	0,	0,	'199602152019021005',	'0f75qfjotbatf7kqrsjdmsnn6tfb8gs0',	'Rizqi'),
('',	3,	1,	1698771600,	1200,	1400,	100,	1573146000,	'199602152019021005',	'lgp9371grau6aa3sfl926bvbj8rhtgvv',	'Rizqi'),
('',	3,	1,	1798736400,	2500,	3500,	1000,	1573146000,	'199602152019021005',	'ih98in3l6og2b0pvnvb0folvs402mke6',	'Rizqi'),
('LAN01',	3,	1,	1767200400,	1600,	2500,	900,	1573146000,	'199602152019021005',	'ih98in3l6og2b0pvnvb0folvs402mke6',	'Rizqi'),
('OBA01',	3,	1,	1596474000,	10000,	13000,	100,	1572800400,	'199602152019021005',	'r6u787e91t0lg4m7cir9q0vm94kv8hlv',	'Rizqi');

DROP TABLE IF EXISTS `permintaan_temp`;
CREATE TABLE `permintaan_temp` (
  `barang_id` char(5) NOT NULL,
  `psd_kredit` float NOT NULL,
  `tanggal` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `hostname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `permintaan_temp`;
INSERT INTO `permintaan_temp` (`barang_id`, `psd_kredit`, `tanggal`, `user_id`, `session_id`, `hostname`) VALUES
('',	0,	1573078319,	'199602152019021005',	'0f75qfjotbatf7kqrsjdmsnn6tfb8gs0',	'Rizqi'),
('OBA01',	200,	1573589387,	'199602152019021005',	'sumebdopavbl6an4ei215vk6i629gtn4',	'Rizqi');

DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `role_menu_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `mst_role` (`role_id`),
  CONSTRAINT `role_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `mst_menu` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `role_menu`;
INSERT INTO `role_menu` (`role_id`, `menu_id`) VALUES
(1,	1),
(1,	2),
(1,	3),
(1,	4),
(1,	5),
(1,	6),
(1,	7),
(1,	8),
(1,	9),
(1,	10),
(1,	11),
(1,	12),
(1,	13),
(1,	14),
(2,	2),
(2,	3),
(2,	4),
(1,	15),
(3,	1),
(3,	2),
(3,	4),
(3,	6),
(3,	7),
(3,	8),
(3,	9),
(3,	11),
(3,	12),
(3,	13),
(3,	15),
(3,	5);

DROP TABLE IF EXISTS `role_users`;
CREATE TABLE `role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `role_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `mst_role` (`role_id`),
  CONSTRAINT `role_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `mst_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `role_users`;
INSERT INTO `role_users` (`role_id`, `user_id`) VALUES
(1,	'199602152019021005'),
(2,	'199602152019021011'),
(3,	'001');

DROP TABLE IF EXISTS `trx_barang`;
CREATE TABLE `trx_barang` (
  `barang_tahunanggaran` char(4) NOT NULL,
  `unit_id` char(4) NOT NULL,
  `subgroup_id` char(2) NOT NULL,
  `barang_id` char(5) NOT NULL,
  `barang_expdate` int(11) NOT NULL,
  `barang_hargasatuan` float NOT NULL,
  `barang_hargajual` float NOT NULL,
  `dana_id` char(2) NOT NULL,
  `created_at` int(11) NOT NULL,
  `barang_tanggal` date NOT NULL,
  `barang_status` char(1) NOT NULL DEFAULT '1' COMMENT '1: Pembelian, 2:Penjualan',
  `barang_nodokumen` varchar(50) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  KEY `unit_id` (`unit_id`),
  KEY `subgroup_id` (`subgroup_id`),
  KEY `barang_id` (`barang_id`),
  KEY `dana_id` (`dana_id`),
  CONSTRAINT `trx_barang_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `mst_unit` (`unit_id`),
  CONSTRAINT `trx_barang_ibfk_2` FOREIGN KEY (`subgroup_id`) REFERENCES `mst_subgroup` (`subgroup_id`),
  CONSTRAINT `trx_barang_ibfk_3` FOREIGN KEY (`barang_id`) REFERENCES `mst_barang` (`barang_id`),
  CONSTRAINT `trx_barang_ibfk_4` FOREIGN KEY (`dana_id`) REFERENCES `mst_dana` (`dana_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `trx_barang`;
INSERT INTO `trx_barang` (`barang_tahunanggaran`, `unit_id`, `subgroup_id`, `barang_id`, `barang_expdate`, `barang_hargasatuan`, `barang_hargajual`, `dana_id`, `created_at`, `barang_tanggal`, `barang_status`, `barang_nodokumen`, `condition_id`, `supplier_id`) VALUES
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572172370,	'2019-10-27',	'1',	'20191027.PEM.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572173116,	'2019-10-27',	'2',	'20191027.PEN.00001',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572173143,	'2019-10-27',	'2',	'20191027.PEN.00002',	1,	3),
('2019',	'F001',	'01',	'FOL01',	1572109200,	500,	700,	'01',	1572173143,	'2019-10-27',	'2',	'20191027.PEN.00002',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532501,	'2019-10-31',	'1',	'20191031.PEM.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532560,	'2019-10-31',	'2',	'20191031.PEN.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532560,	'2019-10-31',	'2',	'20191031.PEN.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532560,	'2019-10-31',	'2',	'20191031.PEN.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532560,	'2019-10-31',	'2',	'20191031.PEN.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532560,	'2019-10-31',	'2',	'20191031.PEN.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532560,	'2019-10-31',	'2',	'20191031.PEN.00001',	1,	3),
('2019',	'F001',	'02',	'OBA01',	1575046800,	500,	700,	'01',	1572532560,	'2019-10-31',	'2',	'20191031.PEN.00001',	1,	3);

DELIMITER ;;

CREATE TRIGGER `trx_barang_stock` AFTER INSERT ON `trx_barang` FOR EACH ROW
IF NEW.barang_status = '1' THEN
UPDATE
    mst_barang 
SET 
    barang_stock = barang_stock + 1 
WHERE 
    barang_id = NEW.barang_id;
ELSEIF NEW.barang_status = '2' THEN
UPDATE
    mst_barang 
SET 
    barang_stock = barang_stock - 1 
WHERE 
    barang_id = NEW.barang_id;
END IF;;

DELIMITER ;

DROP TABLE IF EXISTS `trx_persediaan`;
CREATE TABLE `trx_persediaan` (
  `trx_id` char(3) NOT NULL COMMENT '101: Pembelian. 102: Penjualan',
  `barang_id` char(5) NOT NULL,
  `dana_id` char(2) NOT NULL,
  `psd_tahunperolehan` char(4) NOT NULL,
  `psd_lokasi` varchar(50) NOT NULL,
  `psd_jumlah` int(11) NOT NULL COMMENT 'jumlah stok',
  `psd_debet` float NOT NULL DEFAULT '0',
  `psd_kredit` float NOT NULL DEFAULT '0',
  `psd_harga` float NOT NULL COMMENT 'harga satuan',
  `psd_hargajual` float NOT NULL,
  `psd_d_k` char(1) NOT NULL DEFAULT 'D',
  `psd_nodokumen` varchar(50) NOT NULL,
  `psd_tgldokumen` date NOT NULL,
  `psd_keterangan` mediumtext NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `created_at` int(11) NOT NULL,
  KEY `barang_id` (`barang_id`),
  KEY `dana_id` (`dana_id`),
  KEY `user_id` (`user_id`),
  KEY `suppiler_id` (`supplier_id`),
  CONSTRAINT `trx_persediaan_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `mst_barang` (`barang_id`),
  CONSTRAINT `trx_persediaan_ibfk_2` FOREIGN KEY (`dana_id`) REFERENCES `mst_dana` (`dana_id`),
  CONSTRAINT `trx_persediaan_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `mst_users` (`user_id`),
  CONSTRAINT `trx_persediaan_ibfk_4` FOREIGN KEY (`supplier_id`) REFERENCES `mst_supplier` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE `trx_persediaan`;
INSERT INTO `trx_persediaan` (`trx_id`, `barang_id`, `dana_id`, `psd_tahunperolehan`, `psd_lokasi`, `psd_jumlah`, `psd_debet`, `psd_kredit`, `psd_harga`, `psd_hargajual`, `psd_d_k`, `psd_nodokumen`, `psd_tgldokumen`, `psd_keterangan`, `supplier_id`, `user_id`, `created_at`) VALUES
('101',	'FOL01',	'01',	'2019',	'F001',	10,	10,	0,	500,	700,	'D',	'20191027.PEM.00001',	'2019-10-27',	'Pembelian dari supplier',	3,	'199602152019021005',	1572172370),
('201',	'FOL01',	'01',	'2019',	'F001',	1,	0,	1,	0,	0,	'K',	'20191027.PEN.00001',	'2019-10-27',	'permintaan dari Gudang Farmasi',	3,	'199602152019021005',	1572173116),
('201',	'FOL01',	'01',	'2019',	'F001',	2,	0,	2,	0,	0,	'K',	'20191027.PEN.00002',	'2019-10-27',	'permintaan dari Gudang Farmasi',	3,	'199602152019021005',	1572173143),
('101',	'OBA01',	'01',	'2019',	'F001',	10,	10,	0,	500,	700,	'D',	'20191031.PEM.00001',	'2019-10-31',	'Pembelian dari supplier',	3,	'199602152019021005',	1572532501),
('201',	'OBA01',	'01',	'2019',	'F001',	7,	0,	7,	0,	0,	'K',	'20191031.PEN.00001',	'2019-10-31',	'permintaan dari Gudang Farmasi',	3,	'199602152019021005',	1572532560);

-- 2019-12-06 04:11:11
